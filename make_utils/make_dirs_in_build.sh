#!/bin/bash

# Filename: make_dirs_in_build.sh
# Author: Dragos B. Chirila
# Purpose: Ensure we have the necessary directories in =build=

mkdir -p build/gelbf_runtime_lib
mkdir -p build/lattice_properties
