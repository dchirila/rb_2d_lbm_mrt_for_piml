#!/bin/bash

# Filename: guard_env_vars.sh
# Author: Dragos B. Chirila
# Purpose: Ensure =CUDA_COMPUTE_CAPABILITY= and =BUILD= env-vars are set before invoking =make=

# some colors, for easier-to-recognize error-messages
RED='\033[0;31m'
BLUE='\033[0;34m'

NC='\033[0m'  # no-color

if [ -z "$CUDA_COMPUTE_CAPABILITY" ]; then
    echo -e "${RED}<<<MAKE-ERROR>>>${NC} Environment variable ${RED}CUDA_COMPUTE_CAPABILITY${NC}"
    echo -e "is not set. ${BLUE}<<<HINT>>>${NC} Run ${BLUE}export CUDA_COMPUTE_CAPABILITY=cc75${NC}"
    echo -e "(or pick the highest reported by: ${BLUE}nvfortran -help -gpu${NC})"
    exit 1
fi

if [ -z "$BUILD" ]; then
    echo -e "${RED}<<<MAKE-ERROR>>>${NC} Environment variable ${RED}BUILD${NC}"
    echo -e "is not set. ${BLUE}<<<HINT>>>${NC} Run ${BLUE}export BUILD=release${NC}"
    echo -e "(or: ${BLUE}debug${NC} / ${BLUE}profile${NC})"
    exit 1
fi
