# Filename: common.mk
# Author: Dragos B. Chirila
# Purpose: Common GNU-Make code, which is useful for all GeLB examples

# Useful line for debugging
print-%: ; @echo $*=$($*)

# DEFAULT values for CLI-args
CHECKPOINT_DIR ?= $(shell pwd)
RESUME_FROM_CHECKPOINT ?= "none"
WRITE_CHECKPOINT ?= "no"

# Standard location of build-directory; default is in the =build= sub-directory
# (relative to the GD-file)
build_dir := build
# Standard location of source-code files (the EXAMPLE-SPECIFIC ones!)
src_dir := $(shell pwd)

##############################################
# BEGIN :: trigger running of ENSURE-scripts #
##############################################
# Invokes script which ensures that the mandatory environment-variables have been
# specified by the user
guard_env_vars:
	$(shell ../../make_utils/guard_env_vars.sh)
# Invokes script which ensures that the appropriate (sub)directories are present
# in the =build= directory
make_dirs_in_build:
	-$(shell ../../make_utils/make_dirs_in_build.sh)
############################################
# END :: trigger running of ENSURE-scripts #
############################################

# Common PHONY-targets (i.e. targets which don't lead to the creation of a
# file-artifact)
.PHONY: run stop resclean clean guard_env_vars make_dirs_in_build

# RESET the DEFAULT goal
.DEFAULT_GOAL := $(build_dir)/$(prog)

##########################
# BEGIN :: for UTILs LIB #
##########################
# Standard location of =gelbf_runtime_lib= files
src_dir_gelbf_runtime_lib := ../../gelbf_runtime_lib

srcs_utils_file_paths := $(wildcard $(src_dir_gelbf_runtime_lib)/*.f90)

# strip =$(src_dir_gelbf_runtime_lib)/= prefix
srcs_utils_file_names := $(srcs_utils_file_paths:$(src_dir_gelbf_runtime_lib)/%=%)

objs_utils := $(addprefix $(build_dir)/gelbf_runtime_lib/, $(srcs_utils_file_names:.f90=.o))

utils_lib := $(build_dir)/gelbf_runtime_lib/gelbf_runtime_lib.a

# Everything depends on this file, so we attach the safety shell-scripts to this target
$(build_dir)/gelbf_runtime_lib/NumericKinds.o: guard_env_vars make_dirs_in_build

$(filter-out $(build_dir)/gelbf_runtime_lib/NumericKinds.o, $(objs_utils)): \
	$(build_dir)/gelbf_runtime_lib/NumericKinds.o

# additional dependencies
$(build_dir)/gelbf_runtime_lib/RunTimeUtils.o: \
	$(build_dir)/gelbf_runtime_lib/NetCDFUtils.o \
	$(build_dir)/gelbf_runtime_lib/StringUtils.o \
	$(build_dir)/gelbf_runtime_lib/TimeUtils.o

$(utils_lib): $(objs_utils)
	$(AR) $(AR_OPTS) $(utils_lib) $(objs_utils)

$(build_dir)/gelbf_runtime_lib/%.o: $(src_dir_gelbf_runtime_lib)/%.f90
	$(COMPILE.f) $< $(OUTPUT_OPTION)
########################
# END :: for UTILs LIB #
########################


###################################
# BEGIN :: for lattice_properties #
###################################
# Standard location of =lattice_properties= files
src_dir_lattice_properties := ../../generated_code/lattice_properties

srcs_lattice_properties_file_paths := $(wildcard $(src_dir_lattice_properties)/*.f90)

# strip =$(src_dir_lattice_properties)/= prefix
srcs_lattice_properties_file_names := \
	$(srcs_lattice_properties_file_paths:$(src_dir_lattice_properties)/%=%)

objs_lattice_properties := \
	$(addprefix $(build_dir)/lattice_properties/, $(srcs_lattice_properties_file_names:.f90=.o))

$(build_dir)/lattice_properties/%.o: $(src_dir_lattice_properties)/%.f90 $(utils_lib)
	$(COMPILE.f) $(filter-out $(utils_lib), $<) $(OUTPUT_OPTION)
#################################
# END :: for lattice_properties #
#################################


###################################################
# BEGIN :: generic code for building the examples #
###################################################
srcs_example_file_paths := \
	$(wildcard $(src_dir)/*.f90)

# strip =$(src_dir)/= prefix
srcs_example_file_names := $(srcs_example_file_paths:$(src_dir)/%=%)

objs_example := $(addprefix $(build_dir)/, $(srcs_example_file_names:.f90=.o))

obj_example_main := $(build_dir)/$(prog).o

objs_example_nomain := $(filter-out $(obj_example_main), $(objs_example))

$(build_dir)/$(prog): $(obj_example_main)

$(obj_example_main): $(objs_example_nomain)

$(build_dir)/%.o: $(src_dir)/%.f90 $(objs_lattice_properties)
	$(COMPILE.f) -I$(src_dir) $< $(OUTPUT_OPTION)

$(build_dir)/$(prog): $(objs_example_nomain) $(utils_lib) $(objs_lattice_properties)
	$(LINK.f) $(objs_example) $(utils_lib) $(objs_lattice_properties) $(LOADLIBES) $(LDLIBS) $(OUTPUT_OPTION)
#################################################
# END :: generic code for building the examples #
#################################################


#########################
# BEGIN :: UTIL-targets #
#########################
# UTIL-target: =stop=
stop:
	touch STOP

# UTIL-target: =resclean=
resclean:
	-$(RM) STOP
	-$(RM) result.nc
	-$(RM) checkpoint.nc
	-$(RM) run.log

# UTIL-target: =clean=
clean:
	-$(RM) -r $(build_dir)

# UTIL-target: =run=
run: $(build_dir)/$(prog)
	-$(RM) run.log
	-$(RM) STOP
	ulimit -s unlimited; \
	export CHECKPOINT_DIR=$(CHECKPOINT_DIR); \
	export RESUME_FROM_CHECKPOINT=$(RESUME_FROM_CHECKPOINT); \
	export WRITE_CHECKPOINT=$(WRITE_CHECKPOINT); \
	$(build_dir)/$(prog) 2>&1 | tee run.log
#######################
# END :: UTIL-targets #
#######################

###################################################################
# BEGIN :: support for the various {BUILD}-combinations #
###################################################################
## BEGIN :: =pgigpucudaf=
FC_pgigpucudaf := pgfortran

# NOTE may try to add -Mcuda=...,maxregcount:<N>, to limit the number
#      of registers (which can increase the number of thread-blocks
#      that can concurrently reside on a multiprocessor, which may
#      lead to better latency-hiding). However, for my RB-3D code, this
#      didn't seem to help (probably because the code is memory-bound).
FFLAGS_pgigpucudaf_release := \
	-Mcuda=$(CUDA_COMPUTE_CAPABILITY),ptxinfo \
	-ta=tesla:$(CUDA_COMPUTE_CAPABILITY) -Minfo=accel \
	-fast -Kieee \
	-cpp \
	-Minform=warn \
	-module $(build_dir) \
	-I${subst :, -I,$(INCLUDE)}

FFLAGS_pgigpucudaf_profile := \
	-gopt \
	-Mcuda=$(CUDA_COMPUTE_CAPABILITY),ptxinfo \
	-ta=tesla:$(CUDA_COMPUTE_CAPABILITY),lineinfo -Minfo=accel \
	-fast -Kieee \
	-cpp \
	-Minform=warn \
	-module $(build_dir) \
	-I${subst :, -I,$(INCLUDE)}

FFLAGS_pgigpucudaf_debug := \
	-O0 -g \
	-Mcuda=$(CUDA_COMPUTE_CAPABILITY),ptxinfo \
	-ta=tesla:$(CUDA_COMPUTE_CAPABILITY) -Minfo=accel \
	-Kieee \
	-cpp \
	-Minform=warn \
	-module $(build_dir) \
	-I${subst :, -I,$(INCLUDE)}

DFLAGS_pgigpucudaf := "-DPGI"

AR_pgigpucudaf := ar
AR_OPTS_pgigpucudaf := rcsv
## END :: =pgigpucudaf=
#################################################################
# END :: support for the various {BUILD}-combinations #
#################################################################


# Pick correct toolchain, depending on env vars
FC := ${FC_pgigpucudaf}
DFLAGS := ${DFLAGS_pgigpucudaf}
FFLAGS := ${FFLAGS_pgigpucudaf_${BUILD}} ${DFLAGS}
AR := ${AR_pgigpucudaf}
AR_OPTS := ${AR_OPTS_pgigpucudaf}

# Settings for libs to link against (same for all toolchains)
LDLIBS := \
	-L$(build_dir)/utils \
	-L${subst :, -L,$(LIBRARY_PATH)} \
	-lnetcdff -lnetcdf
