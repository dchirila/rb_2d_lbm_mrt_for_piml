#!/bin/bash

# Clean-up for all example-applications

# Abort on errors
set -e

# Run safety-checks
./make_utils/guard_env_vars.sh

# Save PWD
root_dir=${PWD}

# Iterate over sub-directories of =examples=
for example_dir in examples/*
do
    cd ${root_dir}
    echo "<<<INFO>>> Found example-dir: ${example_dir}"
    cd ${example_dir}
    make clean
    echo "<<<INFO>>> CLEANUP @${example_dir} complete!"
    echo
done
