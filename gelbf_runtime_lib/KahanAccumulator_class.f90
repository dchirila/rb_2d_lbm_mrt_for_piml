module KahanAccumulator_class
  use NumericKinds, only : R_DP
  implicit none

  type KahanAccumulator
     real(R_DP) :: mSum = 0, mC = 0, mY = 0, mT = 0
   contains
     procedure :: addValue
     procedure :: getSum
     procedure :: reset
  end type KahanAccumulator

contains
  subroutine addValue( this, newValue )
    class(KahanAccumulator), intent(inout) :: this
    real(R_DP), intent(in) :: newValue

    this%mY = newValue - this%mC
    this%mT = this%mSum + this%mY
    this%mC = (this%mT - this%mSum) - this%mY
    this%mSum = this%mT
  end subroutine addValue

  real(R_DP) function getSum( this )
    class(KahanAccumulator), intent(in) :: this
    getSum = this%mSum
  end function getSum

  subroutine reset( this )
    class(KahanAccumulator), intent(inout) :: this
    this%mSum = 0
    this%mC = 0
    this%mY = 0
    this%mT = 0
  end subroutine reset
end module KahanAccumulator_class
