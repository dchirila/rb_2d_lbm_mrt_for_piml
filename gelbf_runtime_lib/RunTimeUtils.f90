! File: RunTimeUtils.f90
! Author: Dragos B. Chirila
! Purpose: some useful functions related to the execution of
!          =GeLB= simulations

module RunTimeUtils
  use NumericKinds, only : I4b, I8b, R_SP, R_DP, RK
  use NetCDFUtils
  use StringUtils
  use TimeUtils
  implicit none

contains
  ! Given a number of BYTEs, return a string containing a human-friendly
  ! representation, using the highest unit (in IEC units)
  ! NOTE The build-up of the representation is conceptually similar
  !      to what we have in =seconds2weeks_days_hours_minutes_seconds=
  !      (@=TimeUtils.f90=), EXCEPT that here we only display the
  !      value with the largest unit
  function bytes_to_human_friendly_string( nBytes ) result(res)
    ! interface
    integer(I8b), intent(in) :: nBytes
    character(len=:), allocatable :: res
    ! local type (for array of strings)
    type string
       character(len=:), allocatable :: str
    end type string
    ! local constants
    integer(I8b), dimension(7), parameter :: UNIT_SIZES = &
         [ &  ! Bytes in:
         1024_I8b**0, &  ! 1 Byte
         1024_I8b**1, &  ! 1 KibiByte
         1024_I8b**2, &  ! 1 MebiByte
         1024_I8b**3, &  ! 1 GibiByte
         1024_I8b**4, &  ! 1 TebiByte
         1024_I8b**5, &  ! 1 PebiByte
         1024_I8b**6 ]   ! 1 ExbiByte
    type(string) :: unit_names(7), unit_descriptions(7)
    ! local vars
    integer(I8b) :: bytes=0
    character(256) :: aux_string  ! internal file
    integer idx

    unit_names = [ &
         string("[Bytes]"), &
         string("[KibiBytes]"), &
         string("[MebiBytes]"), &
         string("[GibiBytes]"), &
         string("[TebiBytes]"), &
         string("[PebiBytes]"), &
         string("[ExbiBytes]")]

    unit_descriptions = [ &
         string(""), &
         string("(1 KiB = 1024**1 Bytes)"), &
         string("(1 MiB = 1024**2 Bytes)"), &
         string("(1 GiB = 1024**3 Bytes)"), &
         string("(1 TiB = 1024**4 Bytes)"), &
         string("(1 PiB = 1024**5 Bytes)"), &
         string("(1 EiB = 1024**6 Bytes)")]

    if( nBytes == 0 ) then
       res = trim(adjustl("0 Bytes"))
       return
    else
       ! Strategy: cycle through units (from largest to smallest),
       !           and return as soon as we have a match
       bytes = nBytes
       do idx=7, 1, -1
          if( bytes .ge. UNIT_SIZES(idx) ) then
             write(aux_string, '(a, f0.2, 1x, a)') &
                  "~", &
                  real(bytes) / real(UNIT_SIZES(idx)), &
                  unit_names(idx)%str
             res = colorize(trim(adjustl(aux_string)), red) // &
                  " " // unit_descriptions(idx)%str
             return
          end if
       end do
    end if
  end function bytes_to_human_friendly_string

  ! Estimate maximum space-usage (in bytes) for a Gauge, using the formula:
  ! nNodes * sizeof(ncTypeID) * nMaxSlices * nGaugeFields
  ! TODO generalize this to the case when different =Gauge=s have different
  !      element-sizes (so far, we assume they all push the same data-type
  !      to disk)
  function max_bytes_on_disk( nNodes, ncTypeID, nMaxSlices, nGaugeFields )
    ! interface
    integer(I8b), intent(in) :: nNodes, nMaxSlices, nGaugeFields
    integer, intent(in) :: ncTypeID
    integer(I8b) :: max_bytes_on_disk
    ! local vars
    integer(I8b) :: nScalars

    nScalars = nNodes*nMaxSlices*nGaugeFields

    select case( ncTypeID )
    case(NF_BYTE)  ! 1 byte
       max_bytes_on_disk = nScalars * 1
    case(NF_CHAR)  ! 1 byte
       max_bytes_on_disk = nScalars * 1
    case(NF_SHORT)  ! 2 bytes
       max_bytes_on_disk = nScalars * 2
    case(NF_INT)  ! 4 bytes
       max_bytes_on_disk = nScalars * 4
    case(NF_FLOAT)  ! 4 bytes
       max_bytes_on_disk = nScalars * 4
    case(NF_DOUBLE)  ! 8 bytes
       max_bytes_on_disk = nScalars * 8
    case default
       write(*,'(a)') "<<<ERROR>>> Invalid value for =ncTypeID=. Aborting..."
       stop
    end select
  end function max_bytes_on_disk

  ! Display useful info related to the expected requirements
  ! of the simulation:
  ! - running time (@dcstrix-gpu & @ollie)
  ! - RAM
  ! - space on disk (for all Gauges)
  subroutine print_simulation_requirements( nNodes, ncTypeID, &
       nMaxSlices, nGaugeFields, nMaxIters )
    ! interface
    integer(I8b), intent(in) :: nNodes, nMaxSlices, nGaugeFields, &
         nMaxIters
    integer, intent(in) :: ncTypeID
    ! some local constants
    ! TODO after running a test simulation, change the `speedEstim` value
    real(RK), parameter :: speedEstim = 490._RK
    ! local vars
    integer(I8b) :: seconds, totalLUPS

    write(*,'(a)') "<<<INFO>>> " // colorize( &
         "EXPECTED simulation REQUIREMENTS", red) // ":"
    write(*,'(4x, a)') "- computing " // colorize("TIME", red) // &
         ": (excluding init & teardown):"
    totalLUPS = nMaxIters * nNodes
    seconds = int(totalLUPS / (speedEstim*1.e6), I8b)
    write(*,'(8x, a, f0.2, a, a)') &
         "@dczen-Nvidia-RTX-2060-GPU(", speedEstim, " MLUPS): ", &
         colorize( &
         seconds2weeks_days_hours_minutes_seconds(seconds), &
         red)

    write(*,'(4x, a)') "- " // colorize("RAM", red) // &
         ": " // bytes_to_human_friendly_string( &
          ((storage_size(1._RK)/8) * nNodes*2*(9+5)) + &  ! DFs
          ((storage_size(1._R_SP)/8) * nNodes*4)) ! rawMoments

    write(*,'(4x, a, 1/)') "- " // colorize("STORAGE", red) // &
         " (for output): " // bytes_to_human_friendly_string( &
          max_bytes_on_disk( &
          nNodes, &
          ncTypeID, &
          nMaxSlices, &
          nGaugeFields))  ! pressure + velocity (u, v, w) + temperature
  end subroutine print_simulation_requirements

  ! Simple progress-monitor
  subroutine print_progress( nCurrIter, nItersMax )
    integer(I8b), intent(in) :: nCurrIter, nItersMax
    real(R_SP), save :: lastProgress = 0

    if(nItersMax > 11) then
       if( mod(nCurrIter-1, (nItersMax-1)/10) == 0 ) then
          write(*, '(i5,a)') nint(lastProgress*100), "%"
          lastProgress = lastProgress + 0.1
       else if( (real(nCurrIter-1) / real(nItersMax-1)) > &
            (lastProgress + 0.1) ) then
          ! Also show nice percentages (though they are fake) when
          ! nItersMax doesn't divide nicely by 10
          write(*, '(i5,a)') nint(lastProgress*100), "%"
          lastProgress = lastProgress + 0.1
       end if
    else
       ! less than 11 iterations, so we might as well show progress at each
       ! time-step
       write(*, '(i5,a)') nint((nCurrIter*100._RK)/nItersMax), "%"
    end if
  end subroutine print_progress

  ! BEGIN :: some fancy text, for showing progress with the phases
  subroutine print_mark_init_begin()
    write(*,'(/,a)') colorize( &
         "<<<<<<<<<<<<< INIT :: BEGIN >>>>>>>>>>>>>", yellow)
  end subroutine print_mark_init_begin

  subroutine print_mark_init_end()
    write(*,'(a,/)') colorize( &
         "<<<<<<<<<<<<< INIT :: END >>>>>>>>>>>>>", yellow)
  end subroutine print_mark_init_end

  subroutine print_mark_solver_begin()
    write(*,'(a)') colorize( &
         "<<<<<<<<<<<<< RUN :: BEGIN >>>>>>>>>>>>>", green)
  end subroutine print_mark_solver_begin

  subroutine print_mark_solver_end()
    write(*,'(a,/)') colorize( &
         "<<<<<<<<<<<<< RUN :: END >>>>>>>>>>>>>", green)
  end subroutine print_mark_solver_end

  subroutine print_mark_finalize_begin()
    write(*,'(a)') colorize( &
         "<<<<<<<<<<<<< FINALIZE & CLEANUP :: BEGIN >>>>>>>>>>>>>", blue)
  end subroutine print_mark_finalize_begin

  subroutine print_mark_finalize_end()
    write(*,'(a,/)') colorize( &
         "<<<<<<<<<<<<< FINALIZE & CLEANUP :: END >>>>>>>>>>>>>", blue)
  end subroutine print_mark_finalize_end
  ! END :: some fancy text, for showing progress with the phases

  ! Function to check if STOP-file exists
  logical function stop_file_exists()
    inquire(file="STOP", exist=stop_file_exists)
  end function stop_file_exists

  ! Subroutine to parse ENVIRONMENT variables
  subroutine process_env_vars(checkpoint_dir, write_checkpoint, &
       resume_from_checkpoint)
    implicit none
    ! interface
    character(len=:), allocatable, intent(out) :: checkpoint_dir, &
         resume_from_checkpoint, write_checkpoint
    integer :: str_len, stat

    ! BEGIN :: CHECKPOINT_DIR
    ! ... 1st call: get length of argument
    call get_environment_variable(name="CHECKPOINT_DIR", length=str_len, &
         status=stat, trim_name=.true.)
    ! ... allocate space for argument
    allocate(character(len=str_len) :: checkpoint_dir)
    ! ... 2nd call: get value of argument
    call get_environment_variable(name="CHECKPOINT_DIR", &
         value=checkpoint_dir, length=str_len, &
         status=stat, trim_name=.true.)
    ! END :: CHECKPOINT_DIR

    ! BEGIN :: RESUME_FROM_CHECKPOINT
    ! ... 1st call: get length of argument
    call get_environment_variable(name="RESUME_FROM_CHECKPOINT", &
         length=str_len, status=stat, trim_name=.true.)
    ! ... allocate space for argument
    allocate(character(len=str_len) :: resume_from_checkpoint)
    ! ... 2nd call: get value of argument
    call get_environment_variable(name="RESUME_FROM_CHECKPOINT", &
         value=resume_from_checkpoint, length=str_len, &
         status=stat, trim_name=.true.)
    ! END :: RESUME_FROM_CHECKPOINT

    ! BEGIN :: WRITE_CHECKPOINT
    ! ... 1st call: get length of argument
    call get_environment_variable(name="WRITE_CHECKPOINT", &
         length=str_len, status=stat, trim_name=.true.)
    ! ... allocate space for argument
    allocate(character(len=str_len) :: write_checkpoint)
    ! ... 2nd call: get value of argument
    call get_environment_variable(name="WRITE_CHECKPOINT", &
         value=write_checkpoint, length=str_len, &
         status=stat, trim_name=.true.)
    ! END :: WRITE_CHECKPOINT
  end subroutine process_env_vars
end module RunTimeUtils
