! File: TimeUtils.f90
! Author: Dragos B. Chirila

module TimeUtils
  use NumericKinds
  implicit none

contains
  ! Purpose: given an (integer) number of seconds, return a string with a
  !          human-friendly representation, along the lines:
  !          x [hour(s)] y [day(s)] z [hour(s)] t [minute(s)] u [second(s)]
  ! NOTE if {x/y/z/t/u} is zero, the corresponding unit is skipped, EXCEPT
  !      when {x & y & z & t & u} are all zero (then we return " < 1 [second]")
  function seconds2weeks_days_hours_minutes_seconds( seconds_in ) result(res)
    integer(I8b), intent(in) :: seconds_in
    character(len=:), allocatable :: res
    ! local constants
    integer(I8b), parameter :: SEC_IN_MIN = 60, &
         SEC_IN_HOUR = 60*SEC_IN_MIN, &
         SEC_IN_DAY = 24*SEC_IN_HOUR, &
         SEC_IN_WEEK = 7*SEC_IN_DAY
    ! local vars
    integer(I8b) :: weeks=0, days=0, hours=0, minutes=0, seconds=0
    character(256) :: aux_string  ! internal file

    ! clear string at each function-invocation (otherwise =save=d!)
    aux_string = ""

    seconds = seconds_in
    weeks = seconds / SEC_IN_WEEK
    seconds = mod(seconds, SEC_IN_WEEK)
    days = seconds / SEC_IN_DAY
    seconds = mod(seconds, SEC_IN_DAY)
    hours = seconds / SEC_IN_HOUR
    seconds = mod(seconds, SEC_IN_HOUR)
    minutes = seconds / SEC_IN_MIN
    seconds = mod(seconds, SEC_IN_MIN)
    ! display final hierarchy
    if( weeks > 0 ) then
       if( weeks == 1 ) then
          write(aux_string, '(i0, a)') weeks, " [week]"
       else
          write(aux_string, '(i0, a)') weeks, " [weeks]"
       end if
    end if
    if( days > 0 ) then
       if( days == 1 ) then
          write(aux_string, '(a, 1x, i0, a)') trim(adjustl(aux_string)), &
               days, " [day]"
       else
          write(aux_string, '(a, 1x, i0, a)') trim(adjustl(aux_string)), &
               days, " [days]"
       end if
    end if
    if( hours > 0 ) then
       if( hours == 1 ) then
          write(aux_string, '(a, 1x, i0, a)') trim(adjustl(aux_string)), &
               hours, " [hour]"
       else
          write(aux_string, '(a, 1x, i0, a)') trim(adjustl(aux_string)), &
               hours, " [hours]"
       end if
    end if
    if( minutes > 0 ) then
       if( minutes == 1 ) then
          write(aux_string, '(a, 1x, i0, a)') trim(adjustl(aux_string)), &
               minutes, " [minute]"
       else
          write(aux_string, '(a, 1x, i0, a)') trim(adjustl(aux_string)), &
               minutes, " [minutes]"
       end if
    end if
    if( seconds > 0 ) then
       if( seconds == 1 ) then
          write(aux_string, '(a, 1x, i0, a)') trim(adjustl(aux_string)), &
               seconds, " [second]"
       else
          write(aux_string, '(a, 1x, i0, a)') trim(adjustl(aux_string)), &
               seconds, " [seconds]"
       end if
    end if
    ! return something even if time is less than one second
    if( len(trim(adjustl(aux_string))) == 0 ) then
       write(aux_string, '(1x, a)') "< 1 [second]"
    end if

    res = trim(adjustl(aux_string))  ! automatic allocation
  end function seconds2weeks_days_hours_minutes_seconds
end module TimeUtils
