! File: NetCDFUtils.f90
! Author: Dragos B. Chirila
! Purpose: Some wrapper-functions, to make the use of the NetCDF-F77 API a
!          little more pleasant.

module NetCDFUtils
  use NumericKinds, only : I4b, I8b, R_SP, R_DP
  implicit none

  ! Load NetCDF-F77 API (fallback, because NetCDF-F90 doesn't work @Ollie with
  ! the Cray compilers)
  include 'netcdf.inc'

  real(R_SP), parameter :: GELB_MISSING_VAL_R_SP = -999999
  real(R_DP), parameter :: GELB_MISSING_VAL_R_DP = -999999

contains
  ! Simple subroutine, to validate the =outputFrequency=
  ! NOTE this is trivial, but needed by all Gauges, so it still makes sense
  !      to have a subroutine just for this
  subroutine validate_output_frequency( outputFrequency )
    integer(I8b), intent(in) :: outputFrequency

    if( outputFrequency < 0 ) then
       write(*,'(3(a,/),a)') "<<<ERROR>>> invalid output-parameter", &
            "Cause: =outputFrequency= < 0 (should be >= 0) !", &
            "Fixes: increase =outputFrequency=", &
            "Aborting..."
       stop
    end if
  end subroutine validate_output_frequency

  ! ================ BEGIN :: WRAPPER ===================
  ! ===== BEGIN :: HIGH-LEVEL
  ! CREATE the file
  ! NOTE use =ior= to combine values for =cmode=; some commone ones are:
  ! - NF_CLOBBER -> overwrite the file (if it already exists)
  ! - NF_64BIT_OFFSET -> enable 64bit-offset-mode
  subroutine nfgelb_create(path, cmode, ncid)
    ! interface
    character(len=*), intent(in) :: path
    integer(NF_INT), intent(in) :: cmode
    integer(NF_INT), intent(out) :: ncid
    ! forward call
    call ncCheck( nf_create(path, cmode, ncid) )
  end subroutine nfgelb_create

  ! END DEFINE-MODE
  subroutine nfgelb_end_define_mode(ncid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    ! forward call
    call ncCheck( nf_enddef(ncid) )
  end subroutine nfgelb_end_define_mode

  ! CLOSE the file
  subroutine nfgelb_close(ncid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    call ncCheck( nf_close(ncid) )
  end subroutine nfgelb_close

  ! OPEN the file
  subroutine nfgelb_open(path, cmode, ncid)
    ! interface
    character(len=*), intent(in) :: path
    integer(NF_INT), intent(in) :: cmode
    integer(NF_INT), intent(out) :: ncid
    ! forward call
    call ncCheck( nf_open(path, cmode, ncid) )
  end subroutine nfgelb_open

  ! SYNC file (s.t. OTHER PROCESSES can READ while the program is still WRITING,
  ! e.g. for monitoring & visualizing the simulations)
  subroutine nfgelb_sync_file(ncid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    ! forward call
    call ncCheck( nf_sync(ncid) )
  end subroutine nfgelb_sync_file
  ! ===== END :: HIGH-LEVEL


  ! ===== BEGIN :: ATTRIBUTEs
  ! ===== BEGIN :: ATTRIBUTEs :: GLOBAL
  ! PUT GLOBAL text-attribute
  subroutine nfgelb_put_global_att_text(ncid, name, text)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    character(len=*), intent(in) :: text
    ! forward call
    call ncCheck( nf_put_att_text(ncid, NF_GLOBAL, name, len(text), text) )
  end subroutine nfgelb_put_global_att_text

  ! PUT GLOBAL real (FLOAT) attribute
  subroutine nfgelb_put_global_att_1_float(ncid, name, val)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    real(R_SP), intent(in) :: val
    ! forward call
    call ncCheck( nf_put_att_real(ncid, NF_GLOBAL, name, NF_FLOAT, 1, val) )
  end subroutine nfgelb_put_global_att_1_float

  ! PUT GLOBAL real (DOUBLE) attribute
  subroutine nfgelb_put_global_att_1_double(ncid, name, val)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    real(R_DP), intent(in) :: val
    ! forward call
    call ncCheck( nf_put_att_double(ncid, NF_GLOBAL, name, NF_DOUBLE, 1, val) )
  end subroutine nfgelb_put_global_att_1_double

  ! GET GLOBAL real (FLOAT) attribute
  function nfgelb_get_global_att_1_float(ncid, name) result(res)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    real(R_SP) :: res
    ! forward call
    call ncCheck( nf_get_att_real(ncid, NF_GLOBAL, name, res) )
  end function nfgelb_get_global_att_1_float

  ! GET GLOBAL real (DOUBLE) attribute
  function nfgelb_get_global_att_1_double(ncid, name) result(res)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    real(R_DP) :: res
    ! forward call
    call ncCheck( nf_get_att_double(ncid, NF_GLOBAL, name, res) )
  end function nfgelb_get_global_att_1_double

  ! PUT GLOBAL integer-attribute
  subroutine nfgelb_put_global_att_1_int(ncid, name, val)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT), intent(in) :: val
    ! forward call
    call ncCheck( nf_put_att_int(ncid, NF_GLOBAL, name, NF_INT, 1, val) )
  end subroutine nfgelb_put_global_att_1_int

  ! GET GLOBAL int attribute
  function nfgelb_get_global_att_1_int(ncid, name) result(res)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT) :: res
    ! forward call
    call ncCheck( nf_get_att_int(ncid, NF_GLOBAL, name, res) )
  end function nfgelb_get_global_att_1_int

  ! PUT GLOBAL real-ARRAY-attribute
  subroutine nfgelb_put_global_att_1d_float(ncid, name, rvals)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    real(R_SP), dimension(:), intent(in) :: rvals
    ! forward call
    call ncCheck( nf_put_att_real(ncid, NF_GLOBAL, name, NF_FLOAT, &
         size(rvals), rvals) )
  end subroutine nfgelb_put_global_att_1d_float

  ! PUT GLOBAL real-ARRAY-attribute
  subroutine nfgelb_put_global_att_1d_double(ncid, name, rvals)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    real(R_DP), dimension(:), intent(in) :: rvals
    ! forward call
    call ncCheck( nf_put_att_double(ncid, NF_GLOBAL, name, NF_DOUBLE, &
         size(rvals), rvals) )
  end subroutine nfgelb_put_global_att_1d_double

  ! GET GLOBAL real(FLOAT)-ARRAY-attribute
  function nfgelb_get_global_att_1d_float(ncid, name) result(res)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    real(R_SP), dimension(:), allocatable :: res
    ! local vars
    integer(NF_INT) :: arraylen
    ! inquire length of the attribute-array
    call ncCheck( nf_inq_attlen(ncid, NF_GLOBAL, name, arraylen) )
    ! allocate memory
    allocate(res(arraylen))
    ! forward call
    call ncCheck( nf_get_att_real(ncid, NF_GLOBAL, name, res) )
  end function nfgelb_get_global_att_1d_float

  ! GET GLOBAL real(DOUBLE)-ARRAY-attribute
  function nfgelb_get_global_att_1d_double(ncid, name) result(res)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    real(R_DP), dimension(:), allocatable :: res
    ! local vars
    integer(NF_INT) :: arraylen
    ! inquire length of the attribute-array
    call ncCheck( nf_inq_attlen(ncid, NF_GLOBAL, name, arraylen) )
    ! allocate memory
    allocate(res(arraylen))
    ! forward call
    call ncCheck( nf_get_att_double(ncid, NF_GLOBAL, name, res) )
  end function nfgelb_get_global_att_1d_double
  ! ===== END :: ATTRIBUTEs :: GLOBAL

  ! ===== BEGIN :: ATTRIBUTEs :: VARIABLEs
  ! Higher-level subroutine, to more easily set the metadata for a variable
  subroutine nfgelb_set_var_metadata(ncid, varid, units, long_name)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    character(len=*), intent(in) :: units
    character(len=*), intent(in) :: long_name
    ! forward callS
    call nfgelb_put_var_att_text(ncid, varid, "units", units)
    call nfgelb_put_var_att_text(ncid, varid, "long_name", long_name)
  end subroutine nfgelb_set_var_metadata

  ! PUT VARIABLE text-attribute
  subroutine nfgelb_put_var_att_text(ncid, varid, name, text)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    character(len=*), intent(in) :: name
    character(len=*), intent(in) :: text
    ! forward call
    call ncCheck( nf_put_att_text(ncid, varid, name, len(text), text) )
  end subroutine nfgelb_put_var_att_text

  ! PUT VARIABLE float-attribute
  subroutine nfgelb_put_var_att_float(ncid, varid, name, rval)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    character(len=*), intent(in) :: name
    real(R_SP), intent(in) :: rval
    ! forward call
    call ncCheck( nf_put_att_real(ncid, varid, name, NF_REAL, 1, rval) )
  end subroutine nfgelb_put_var_att_float
  ! ===== END :: ATTRIBUTEs :: VARIABLEs
  ! ===== END :: ATTRIBUTEs


  ! ===== BEGIN :: Working with DIMENSIONs
  ! Define DIMENSION
  ! NOTE pass =NF_UNLIMITED= as =len= to get an unlimited (=record) dimension
  function nfgelb_define_dimension(ncid, name, len) result(dimid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT), intent(in) :: len
    integer(NF_INT) :: dimid
    ! forward call
    call ncCheck( nf_def_dim(ncid, name, len, dimid) )
  end function nfgelb_define_dimension

  ! Get DIMENSION-array (INT, 1D)
  function nfgelb_read_dimension_array_int(ncid, name) result(res)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT), dimension(:), allocatable :: res
    ! local vars
    integer(NF_INT) :: dimid, dimlen, varid

    ! get dimension-ID
    call ncCheck( nf_inq_dimid(ncid, name, dimid) )
    ! get dimension-length
    call ncCheck( nf_inq_dimlen(ncid, dimid, dimlen) )
    ! allocate array holding the values along the dimension
    allocate(res(dimlen))
    ! get ID of var containing values along the dimension
    call ncCheck( nf_inq_varid(ncid, name, varid) )
    ! read-in values along the dimension
    call ncCheck( nf_get_var_int(ncid, varid, res) )
  end function nfgelb_read_dimension_array_int

  ! Get DIMENSION-array (FLOAT_SP, 1D)
  function nfgelb_read_dimension_array_float_sp(ncid, name) result(res)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    real(R_SP), dimension(:), allocatable :: res
    ! local vars
    integer(NF_INT) :: dimid, dimlen, varid

    ! get dimension-ID
    call ncCheck( nf_inq_dimid(ncid, name, dimid) )
    ! get dimension-length
    call ncCheck( nf_inq_dimlen(ncid, dimid, dimlen) )
    ! allocate array holding the values along the dimension
    allocate(res(dimlen))
    ! get ID of var containing values along the dimension
    call ncCheck( nf_inq_varid(ncid, name, varid) )
    ! read-in values along the dimension
    call ncCheck( nf_get_var_real(ncid, varid, res) )
  end function nfgelb_read_dimension_array_float_sp

  ! Get DIMENSION-array (FLOAT_DP, 1D)
  function nfgelb_read_dimension_array_float_dp(ncid, name) result(res)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    real(R_DP), dimension(:), allocatable :: res
    ! local vars
    integer(NF_INT) :: dimid, dimlen, varid

    ! get dimension-ID
    call ncCheck( nf_inq_dimid(ncid, name, dimid) )
    ! get dimension-length
    call ncCheck( nf_inq_dimlen(ncid, dimid, dimlen) )
    ! allocate array holding the values along the dimension
    allocate(res(dimlen))
    ! get ID of var containing values along the dimension
    call ncCheck( nf_inq_varid(ncid, name, varid) )
    ! read-in values along the dimension
    call ncCheck( nf_get_var_double(ncid, varid, res) )
  end function nfgelb_read_dimension_array_float_dp
  ! ===== END :: Working with DIMENSIONs


  ! ===== BEGIN :: DEFINE VARIABLES
  ! ===== BEGIN :: DEFINE VARIABLES :: 1D
  ! Define 1D VARIABLE (INT)
  function nfgelb_define_var_1d_int(ncid, name, dimid) result(varid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT), intent(in) :: dimid
    integer(NF_INT) :: varid
    ! forward call
    call ncCheck( nf_def_var(ncid, name, NF_INT, 1, dimid, varid) )
  end function nfgelb_define_var_1d_int

  ! Define 1D VARIABLE (FLOAT)
  function nfgelb_define_var_1d_float(ncid, name, dimid) result(varid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT), intent(in) :: dimid
    integer(NF_INT) :: varid
    ! forward call
    call ncCheck( nf_def_var(ncid, name, NF_FLOAT, 1, dimid, varid) )
  end function nfgelb_define_var_1d_float

  ! Define 1D VARIABLE (DOUBLE)
  function nfgelb_define_var_1d_double(ncid, name, dimid) result(varid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT), intent(in) :: dimid
    integer(NF_INT) :: varid
    ! forward call
    call ncCheck( nf_def_var(ncid, name, NF_DOUBLE, 1, dimid, varid) )
  end function nfgelb_define_var_1d_double
  ! ===== END :: DEFINE VARIABLES :: 1D

  ! ===== BEGIN :: DEFINE VARIABLES :: 2D
  ! Define 2D VARIABLE (FLOAT)
  function nfgelb_define_var_2d_float(ncid, name, dimids) result(varid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT), dimension(:), intent(in) :: dimids
    integer(NF_INT) :: varid
    ! forward call
    call ncCheck( nf_def_var(ncid, name, NF_FLOAT, 2, dimids, varid) )
  end function nfgelb_define_var_2d_float

  ! Define 2D VARIABLE (DOUBLE)
  function nfgelb_define_var_2d_double(ncid, name, dimids) result(varid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT), dimension(:), intent(in) :: dimids
    integer(NF_INT) :: varid
    ! forward call
    call ncCheck( nf_def_var(ncid, name, NF_DOUBLE, 2, dimids, varid) )
  end function nfgelb_define_var_2d_double
  ! ===== END :: DEFINE VARIABLES :: 2D

  ! ===== BEGIN :: DEFINE VARIABLES :: 3D
  ! Define 3D VARIABLE (FLOAT)
  function nfgelb_define_var_3d_float(ncid, name, dimids) result(varid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT), dimension(:), intent(in) :: dimids
    integer(NF_INT) :: varid
    ! forward call
    call ncCheck( nf_def_var(ncid, name, NF_FLOAT, 3, dimids, varid) )
  end function nfgelb_define_var_3d_float

  ! Define 3D VARIABLE (DOUBLE)
  function nfgelb_define_var_3d_double(ncid, name, dimids) result(varid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT), dimension(:), intent(in) :: dimids
    integer(NF_INT) :: varid
    ! forward call
    call ncCheck( nf_def_var(ncid, name, NF_DOUBLE, 3, dimids, varid) )
  end function nfgelb_define_var_3d_double
  ! ===== END :: DEFINE VARIABLES :: 3D

  ! ===== BEGIN :: DEFINE VARIABLES :: 4D
  ! Define 4D VARIABLE (FLOAT)
  function nfgelb_define_var_4d_float(ncid, name, dimids) result(varid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT), dimension(:), intent(in) :: dimids
    integer(NF_INT) :: varid
    ! forward call
    call ncCheck( nf_def_var(ncid, name, NF_FLOAT, 4, dimids, varid) )
  end function nfgelb_define_var_4d_float

  ! Define 4D VARIABLE (DOUBLE)
  function nfgelb_define_var_4d_double(ncid, name, dimids) result(varid)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    character(len=*), intent(in) :: name
    integer(NF_INT), dimension(:), intent(in) :: dimids
    integer(NF_INT) :: varid
    ! forward call
    call ncCheck( nf_def_var(ncid, name, NF_DOUBLE, 4, dimids, varid) )
  end function nfgelb_define_var_4d_double
  ! ===== END :: DEFINE VARIABLES :: 4D
  ! ===== END :: DEFINE VARIABLES


  ! ===== BEGIN :: WRITE VARIABLES
  ! ===== BEGIN :: WRITE VARIABLES :: ENTIRE variables
  ! WRITE an ENTIRE VARIABLE (1D, INT)
  subroutine nfgelb_put_entire_var_1d_int(ncid, varid, rvals)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    integer(NF_INT), dimension(:), intent(in) :: rvals
    ! forward call
    call ncCheck( nf_put_var_int(ncid, varid, rvals) )
  end subroutine nfgelb_put_entire_var_1d_int

  ! WRITE an ENTIRE VARIABLE (1D, FLOAT)
  subroutine nfgelb_put_entire_var_1d_float(ncid, varid, rvals)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    real(R_SP), dimension(:), intent(in) :: rvals
    ! forward call
    call ncCheck( nf_put_var_real(ncid, varid, rvals) )
  end subroutine nfgelb_put_entire_var_1d_float

  ! WRITE an ENTIRE VARIABLE (1D, DOUBLE)
  subroutine nfgelb_put_entire_var_1d_double(ncid, varid, rvals)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    real(R_DP), dimension(:), intent(in) :: rvals
    ! forward call
    call ncCheck( nf_put_var_double(ncid, varid, rvals) )
  end subroutine nfgelb_put_entire_var_1d_double

  ! WRITE an ENTIRE VARIABLE (3D, DOUBLE)
  subroutine nfgelb_put_entire_var_3d_double(ncid, varid, rvals)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    real(R_DP), dimension(0:,:,:), intent(in) :: rvals
    ! forward call
    call ncCheck( nf_put_var_double(ncid, varid, rvals) )
  end subroutine nfgelb_put_entire_var_3d_double

  ! WRITE an ENTIRE VARIABLE (4D, FLOAT)
  subroutine nfgelb_put_entire_var_4d_float(ncid, varid, rvals)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    real(R_SP), dimension(0:,:,0:,:), intent(in) :: rvals
    ! forward call
    call ncCheck( nf_put_var_real(ncid, varid, rvals) )
  end subroutine nfgelb_put_entire_var_4d_float

  ! WRITE an ENTIRE VARIABLE (4D, DOUBLE)
  subroutine nfgelb_put_entire_var_4d_double(ncid, varid, rvals)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    real(R_DP), dimension(0:,:,0:,:), intent(in) :: rvals
    ! forward call
    call ncCheck( nf_put_var_double(ncid, varid, rvals) )
  end subroutine nfgelb_put_entire_var_4d_double

  ! READ an ENTIRE VARIABLE (2D, FLOAT)
  ! NOTE subroutine, because we mutate array of caller
  subroutine nfgelb_get_entire_var_2d_float(ncid, name, &
       xFirst, yFirst, array)
    ! interface
    integer(NF_INT), intent(in) :: ncid, xFirst, yFirst
    character(len=*), intent(in) :: name
    real(R_SP), dimension(xFirst:, yFirst:), intent(out) :: &
         array
    ! local vars
    integer(NF_INT) :: varid
    ! get var-ID
    call ncCheck( nf_inq_varid(ncid, name, varid) )
    ! forward call
    call ncCheck( nf_get_var_real(ncid, varid, array) )
  end subroutine nfgelb_get_entire_var_2d_float

  ! READ an ENTIRE VARIABLE (3D, DOUBLE)
  ! NOTE subroutine, because we mutate array of caller
  subroutine nfgelb_get_entire_var_3d_double(ncid, name, &
       qFirst, xFirst, yFirst, array)
    ! interface
    integer(NF_INT), intent(in) :: ncid, qFirst, xFirst, yFirst
    character(len=*), intent(in) :: name
    real(R_DP), dimension(qFirst:, xFirst:, yFirst:), intent(out) :: &
         array
    ! local vars
    integer(NF_INT) :: varid
    ! get var-ID
    call ncCheck( nf_inq_varid(ncid, name, varid) )
    ! forward call
    call ncCheck( nf_get_var_double(ncid, varid, array) )
  end subroutine nfgelb_get_entire_var_3d_double

  ! READ an ENTIRE VARIABLE (4D, FLOAT)
  ! NOTE subroutine, because we mutate array of caller
  subroutine nfgelb_get_entire_var_4d_float(ncid, name, &
       qFirst, xFirst, yFirst, zFirst, array)
    ! interface
    integer(NF_INT), intent(in) :: ncid, qFirst, xFirst, yFirst, zFirst
    character(len=*), intent(in) :: name
    real(R_SP), dimension(qFirst:, xFirst:, yFirst:, zFirst:), intent(out) :: &
         array
    ! local vars
    integer(NF_INT) :: varid
    ! get var-ID
    call ncCheck( nf_inq_varid(ncid, name, varid) )
    ! forward call
    call ncCheck( nf_get_var_real(ncid, varid, array) )
  end subroutine nfgelb_get_entire_var_4d_float

  ! READ an ENTIRE VARIABLE (4D, DOUBLE)
  ! NOTE subroutine, because we mutate array of caller
  subroutine nfgelb_get_entire_var_4d_double(ncid, name, &
       qFirst, xFirst, yFirst, zFirst, array)
    ! interface
    integer(NF_INT), intent(in) :: ncid, qFirst, xFirst, yFirst, zFirst
    character(len=*), intent(in) :: name
    real(R_DP), dimension(qFirst:, xFirst:, yFirst:, zFirst:), intent(out) :: &
         array
    ! local vars
    integer(NF_INT) :: varid
    ! get var-ID
    call ncCheck( nf_inq_varid(ncid, name, varid) )
    ! forward call
    call ncCheck( nf_get_var_double(ncid, varid, array) )
  end subroutine nfgelb_get_entire_var_4d_double
  ! ===== END :: WRITE VARIABLES :: ENTIRE variables

  ! ===== BEGIN :: WRITE VARIABLES :: APPEND to variables
  ! APPEND a SINGLE value to ARRAY-VARIABLE (1D, FLOAT)
  subroutine nfgelb_append_1_to_var_1d_float(ncid, varid, position, rval)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    integer(NF_INT), intent(in) :: position
    real(R_SP), intent(in) :: rval
    ! forward call
    call ncCheck( nf_put_vara_real(ncid, varid, [position], [1], rval) )
  end subroutine nfgelb_append_1_to_var_1d_float

  ! APPEND a SINGLE value to ARRAY-VARIABLE (1D, DOUBLE)
  subroutine nfgelb_append_1_to_var_1d_double(ncid, varid, position, rval)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    integer(NF_INT), intent(in) :: position
    real(R_DP), intent(in) :: rval
    ! forward call
    call ncCheck( nf_put_vara_double(ncid, varid, [position], [1], rval) )
  end subroutine nfgelb_append_1_to_var_1d_double

  ! APPEND a 2D-SLICE to ARRAY-VARIABLE (3D, FLOAT)
  subroutine nfgelb_append_slice_to_var_3d_float(ncid, varid, start, count, rvals)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    integer(NF_INT), dimension(3), intent(in) :: start
    integer(NF_INT), dimension(3), intent(in) :: count
    real(R_SP), dimension(:,:), intent(in) :: rvals
    ! forward call
    call ncCheck( nf_put_vara_real(ncid, varid, start, count, rvals) )
  end subroutine nfgelb_append_slice_to_var_3d_float

  ! APPEND a 3D-SLICE to ARRAY-VARIABLE (4D, FLOAT)
  subroutine nfgelb_append_slice_to_var_4d_float(ncid, varid, start, count, rvals)
    ! interface
    integer(NF_INT), intent(in) :: ncid
    integer(NF_INT), intent(in) :: varid
    integer(NF_INT), dimension(4), intent(in) :: start
    integer(NF_INT), dimension(4), intent(in) :: count
    real(R_SP), dimension(:,:,:), intent(in) :: rvals
    ! forward call
    call ncCheck( nf_put_vara_real(ncid, varid, start, count, rvals) )
  end subroutine nfgelb_append_slice_to_var_4d_float
  ! ===== END :: WRITE VARIABLES :: APPEND to variables
  ! ===== END :: WRITE VARIABLES
  ! ================ END :: WRAPPER ===================


  ! error-checking wrapper for netCDF operations
  subroutine ncCheck( status )
    integer(I4b), intent(in) :: status

    if( status /= nf_noerr ) then
       write(*,'(a)') trim( nf_strerror(status) )
       stop "ERROR (netCDF-related) See message above for details! Aborting..."
    end if
  end subroutine ncCheck
end module NetCDFUtils
