module NumericKinds
  implicit none

  ! KINDs for different types of REALs
  integer, parameter :: &
       R_SP = selected_real_kind(  6,   37 ), &
       R_DP = selected_real_kind( 15,  307 )
  ! Alias for precision that we use in the program (change this to any of the
  ! values 'R_SP' or 'R_DP', to switch to another precision globally).
  integer, parameter :: RK = R_DP ! if changing this, also change RK_FMT

  ! KINDs for different types of INTEGERs
  integer, parameter :: &
       I1b = selected_int_kind(2), &   ! -128 to 127
       I2b = selected_int_kind(4), &   ! -3.2768e4 to 3.2767e4
       I4b = selected_int_kind(9), &   ! -2.147483648e9 to 2.147483647e9
       ! –9.223372036854775808e18 to 9.223372036854775807e18
       I8b = selected_int_kind(18)

  ! Edit-descriptors for real-values
  character(len=*), parameter :: R_SP_FMT = "f0.6", &
       R_DP_FMT = "f0.15"
  ! Alias for output-precision to use in the program (keep this in sync with RK)
  character(len=*), parameter :: RK_FMT = R_DP_FMT

  interface swap ! generic IFACE
     module procedure swapR_SP, swapR_DP, &
          swapI1b, swapI2b, swapI4b, swapI8b
  end interface swap
contains

  ! BEGIN :: SWAP / REAL
  elemental subroutine swapR_SP( a, b )
    real(R_SP), intent(inout) :: a, b
    real(R_SP) :: tmp
    tmp = a; a = b; b = tmp
  end subroutine swapR_SP

  elemental subroutine swapR_DP( a, b )
    real(R_DP), intent(inout) :: a, b
    real(R_DP) :: tmp
    tmp = a; a = b; b = tmp
  end subroutine swapR_DP
  ! END :: SWAP / REAL


  ! BEGIN :: SWAP / INTEGER
  elemental subroutine swapI1b( a, b )
    integer(I1b), intent(inout) :: a, b
    integer(I1b) :: tmp
    tmp = a; a = b; b = tmp
  end subroutine swapI1b

  elemental subroutine swapI2b( a, b )
    integer(I2b), intent(inout) :: a, b
    integer(I2b) :: tmp
    tmp = a; a = b; b = tmp
  end subroutine swapI2b

  elemental subroutine swapI4b( a, b )
    integer(I4b), intent(inout) :: a, b
    integer(I4b) :: tmp
    tmp = a; a = b; b = tmp
  end subroutine swapI4b

  elemental subroutine swapI8b( a, b )
    integer(I8b), intent(inout) :: a, b
    integer(I8b) :: tmp
    tmp = a; a = b; b = tmp
  end subroutine swapI8b
  ! END :: SWAP / INTEGER
end module NumericKinds
