! File: StringUtils.f90
! Author: Dragos B. Chirila
! Source: Inspired by the =ansi_colors= example by Jason Blevins
!         (http://fortranwiki.org/fortran/show/ansi_colors)

module StringUtils
  implicit none
  ! BEGIN :: actual colors
  character(len=*), parameter :: black = '30'
  character(len=*), parameter :: red = '31'
  character(len=*), parameter :: green = '32'
  character(len=*), parameter :: yellow = '33'
  character(len=*), parameter :: blue = '34'
  character(len=*), parameter :: magenta = '35'
  character(len=*), parameter :: cyan = '36'
  character(len=*), parameter :: white = '37'
  ! END :: actual colors

  ! BEGIN :: control codes
  character(len=1), parameter, private :: &
       code_esc = achar(27)
  character(len=2), parameter, private :: &
       code_start = code_esc // '['
  character(len=1), parameter, private :: &
       code_end = 'm'
  character(len=*), parameter, private :: &
       code_clear = code_start // '0' // code_end
  ! END :: control codes

contains

  ! Purpose: return a colorized version of =string_in=, in the
  !          color requested by the caller
  function colorize(string_in, color) result(string_out)
    character(len=*), intent(in) :: string_in
    character(len=*), intent(in) :: color
    character(len=:), allocatable :: string_out
    string_out = &
         code_start // color // code_end // string_in &
         // code_clear
  end function colorize
end module StringUtils
