#!/bin/bash

# Builds all example-applications

# Abort on errors
set -e

# Run safety-checks
./make_utils/guard_env_vars.sh

# Save PWD
root_dir=${PWD}

# Iterate over sub-directories of =examples=
for example_dir in examples/*
do
    echo $TOOLCHAIN
    cd ${root_dir}
    echo "<<<INFO>>> Found example-dir: ${example_dir}"
    cd ${example_dir}
    make
    echo "<<<INFO>>> BUILD @${example_dir} complete!"
    echo
done
