module LbmConstantsMrtD3Q7
  use NumericKinds, only : I4b, RK
  implicit none

  integer(I4b), dimension(3, 0:6), parameter :: EV_TEMP = reshape( &
       source = [ &
       0,  0,  0, &
       1,  0,  0, &
       -1,  0,  0, &
       0,  1,  0, &
       0, -1,  0, &
       0,  0,  1, &
       0,  0, -1],&
       shape = [3, 7])

  integer(I4b), dimension(0:6), parameter :: OPPOSITE_TEMP = &
       [0 , 2 , 1 , 4 , 3 , 6 , 5]

  real(RK), dimension(0:6, 0:6), parameter :: N_TEMP = reshape( &
       source = [ &
       1,  1,  1,  1,  1,  1,  1, &
       0,  1, -1,  0,  0,  0,  0, &
       0,  0,  0,  1, -1,  0,  0, &
       0,  0,  0,  0,  0,  1, -1, &
      -6,  1,  1,  1,  1,  1,  1, &
       0,  1,  1, -1, -1,  0,  0, &
       0,  1,  1,  1,  1, -2, -2], &
       shape = [7, 7])

  ! NOTE: the inverse can also be computed programatically as:
  !       $N \times (N \times N^T)^{-1}$.
  !       However, we hard-code it here, to make this matrix a constant too
  !       (which enables some code-optimization by the compilers).
  real(RK), dimension(0:6, 0:6), parameter :: N_INV_TEMP = reshape( &
       source = [ &
       1/7.,    0.,    0.,    0., -1/7.,    0.,    0., &
       1/7.,  1/2.,    0.,    0., 1/42.,  1/4., 1/12., &
       1/7., -1/2.,    0.,    0., 1/42.,  1/4., 1/12., &
       1/7.,    0.,  1/2.,    0., 1/42., -1/4., 1/12., &
       1/7.,    0., -1/2.,    0., 1/42., -1/4., 1/12., &
       1/7.,    0.,    0.,  1/2., 1/42.,    0., -1/6., &
       1/7.,    0.,    0., -1/2., 1/42.,    0., -1/6.], &
       shape = [7, 7])
end module LbmConstantsMrtD3Q7
