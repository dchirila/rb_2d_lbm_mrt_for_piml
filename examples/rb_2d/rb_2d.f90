! File: rb_2d.f90
!
! Purpose: "DRIVER" program for a 2D incompressible Navier-Stokes equation
! (INSE) solver, with a thermal component coupled using the Boussinesq
! approximation. For simulating the evolution of the (athermal) fluid, and of
! the temperature-field, we use the lattice Boltzmann method (LBM), with
! multi-relaxation-time (MRT) collision operators.
!
! Geometry: rectangular channel, with aspect-ratio
! $\gamma \equiv \frac{L_x}{L_y} = 4.0316$
!
! Boundary Conditions (BCs):
! - periodic domain-wrapping along X-direction (for both fluid- and
!   temperature-solvers)
! - fluid: no-slip (achieved through "bounce-back") at top and bottom walls
! - temperature: constant temperature (achieved through "anti-bounce-back") at
!   top and bottom walls; the lower wall is maintained at a higher temperature,
!   to allow convection to occur
!
! Initial Conditions (ICs):
! - fluid: at rest and without any pressure-gradients
! - temperature: linear temperature-profile (to speed-up the simulations), plus
!   a small perturbation near the center of the bottom wall, to break the
!   symmetry
!
! Author: Dragos B. Chirila

program rb_2d
  use omp_lib
  use NumericKinds, only : I4b, I8b, RK
  use StringUtils
  use MrtSolverBoussinesq2D_class
  use OutputFieldsNetcdf_class
  implicit none
  ! BEGIN :: PARAMs which DON'T change often
  real(RK), parameter :: &
       PI = 4*atan(1.0_RK), &
       ! To allow the 1st instability to develop, the aspect-ratio needs to be a
       ! multiple of $\frac{2 \pi}{k_C}$, where $k_C = 3.117$ (see [Shan1997]).
       K_C = 3.117, &
       ASPECT_RATIO = 7 ! (2*PI)/K_C
  ! END :: PARAMs which DON'T change often
  ! BEGIN :: PARAMs which change more often
  real(RK), parameter :: &
       Ra = 2.500e8_RK, &
       Pr = 0.71_RK, &
       simTime = 0.400_RK, &
       maxMach = 0.1_RK
  integer, parameter :: Ny = 256
  integer(I8b), parameter :: outputFrequency=1000_I8b
  character(len=*), parameter :: outFilePrefix="result"
  ! END :: PARAMs which change more often
  ! local vars
  integer(I4b) :: Nx  ! lattice sizes (which are not constant)
  integer(I8b) :: numItersMax, nMaxSlices=0, currIterNum
  character(len=:), allocatable :: checkpoint_dir, resume_from_checkpoint, &
       write_checkpoint
  character(len=1024) :: checkpoint_file_name
  real(RK) :: tic, toc, numMLUPS, &  ! for measuring actual performance
       dxD, dtD, alphaG, viscosity, diffusivity  ! DIMLESS -> NUMERIC
  type(MrtSolverBoussinesq2D) :: solver  ! associated solver...
  type(OutputFieldsNetcdf) :: outSink    ! ...and output-writer

  ! ======== BEGIN :: COMPILER-SPECIFIC QUIRKs ======== !
#ifdef PGI
  ! Cause the output-buffers to be flushed after each =write= or =print=
  ! statement (s.t. we see the output immediately in the terminal, instead of
  ! only showing it at the end of the simulation)
  call setvbuf3f(6, 2, 0)
#endif  ! PGI
  ! ======== END :: COMPILER-SPECIFIC QUIRKs ======== !

  ! ======== BEGIN :: INIT ======== !
  ! ... compute derived parameters
  ! ... ... final lattice size
  Nx = nint(ASPECT_RATIO * Ny, I4b)
  ! ... ... scaling of "dimensionless" params to "numeric" system
  dxD = 1._RK / Ny
  alphaG = (maxMach**2) / (3._RK * Ny)
  dtD = maxMach / (Ny * sqrt(3._RK * Ra * Pr))
  viscosity = Ny * maxMach * sqrt(Pr/(3._RK*Ra))
  diffusivity = (Ny * maxMach) / sqrt(3._RK*Ra*Pr)
  numItersMax = nint(simTime / dtD, I8b)

  call print_mark_init_begin()

  write(*,'(a, f0.6)') "<<<INFO>>> ASPECT_RATIO = ", ASPECT_RATIO
  write(*,'(a, 2(i0, a))') "<<<INFO>>> Mesh size = {Nx : ", Nx, &
       ", Ny : ", Ny, "}"
  write(*,'(a,i0,1/)') "<<<INFO>>> nItersMax = ", numItersMax

  ! ... calculate max number of output-slices
  if(outputFrequency > 0) then  ! avoid divide-by-zero @no-output
     nMaxSlices = (numItersMax / outputFrequency) + 1  ! "+1" due to ICs
  end if

  call process_env_vars(checkpoint_dir, write_checkpoint, resume_from_checkpoint)

  call print_simulation_requirements( &
       nNodes=int(Nx * Ny, I8b), &
       ncTypeID=NF_FLOAT, &
       nMaxSlices=nMaxSlices, &
       nGaugeFields=4_I8b, &  ! pressure + velocity (u, v) + temperature
       nMaxIters=numItersMax)

  call outSink%init(Nx, Ny, outputFrequency, dxD, dtD, &
       numItersMax, outFilePrefix, Ra, Pr, maxMach)

  call solver%init(Nx, Ny, alphaG, viscosity, diffusivity)

  if (resume_from_checkpoint /= "none") then
     write(*,'(a)',advance='no') '<<<INFO>>> Attempting to resume from' // &
          ' CHECKPOINT-file: "' // resume_from_checkpoint // '"...'
     call solver%loadCheckpoint(resume_from_checkpoint)
     write(*,'(a)') 'OK'
  end if

  call outSink%writeOutput(solver%mRawMomDRho, &
       solver%mRawMomUx, solver%mRawMomUy, &
       solver%mRawMomTemp, 0_I8b)

  call print_mark_init_end()
  ! ======== END :: INIT ======== !

  ! ======== BEGIN :: RUN ======== !
  call print_mark_solver_begin()

  tic = omp_get_wtime() ! parallel

  ! MAIN loop (time-iteration)
  do currIterNum=1, numItersMax
     if( stop_file_exists() ) exit  ! gracefully stop

     call print_progress(currIterNum, numItersMax)

     ! NOTE GPU-based solver needs to know whether we will write
     !      output during this iteration, s.t. it can transfer
     !      the raw-macros back to the host!
     call solver%advanceTime(outSink%isTimeToWrite(currIterNum))

     call outSink%writeOutput(solver%mRawMomDRho, &
          solver%mRawMomUx, solver%mRawMomUy, &
          solver%mRawMomTemp, currIterNum )
  end do

  if( write_checkpoint /= "no" ) then  ! CHECKPOINT-write enabled?
     ! write CHECKPOINT after last iteration
     ! ... create checkpoint-filename
     checkpoint_file_name = ""
     write(checkpoint_file_name,'(a,i0,a)') "checkpoint_", &
          currIterNum, ".nc"
     call solver%saveCheckpoint(trim(checkpoint_file_name))
  end if

  toc = omp_get_wtime() ! parallel

  call print_mark_solver_end()

  numMLUPS = currIterNum*real(Nx * Ny, RK) / (1.0e6*(toc-tic))
  write(*,'(a,f0.2,a)') "<<<INFO>>> Actual performance = ", &
       numMLUPS, " MLUPS (mega-lattice-updates-per-second)"
  write(*,'(a,f0.4,a,1/)') "[ <perf> ", numMLUPS, "</perf> ]"
  ! ======== END :: RUN ======== !

  ! ======== BEGIN :: CLEANUP ======== !
  call print_mark_finalize_begin()
  call solver%cleanup()
  call outSink%cleanup()
  call print_mark_finalize_end()
  ! ======== END :: CLEANUP ======== !
end program rb_2d
