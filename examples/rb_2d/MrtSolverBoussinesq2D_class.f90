module MrtSolverBoussinesq2D_class
  use cudafor
  use NumericKinds, only : I4b, RK, R_SP, R_DP, swap
  use NetCDFUtils
  use KahanAccumulator_class
  use LbmConstantsMrtD2Q5
  use LbmConstantsMrtD2Q9
  implicit none

  real(RK), parameter :: epsD = 0.25_RK, &  ! for temperature-solver
       ! See [Wang2013] for justification of these parameters.
       SIGMA_K = 3._RK - sqrt(3._RK), &
       SIGMA_NU_E = 2._RK * (2._RK*sqrt(3._RK) - 3._RK), &
       ! plane-temperatures are always the same in "numeric" units
       TEMP_COLD_WALL = 0._RK, TEMP_HOT_WALL = 1._RK

  type :: MrtSolverBoussinesq2D
    private
    ! parameters for the algorithm (not bound to the RB-setup)
    ! ... CPU
    real(RK) :: mAlphaG, mAParam, mDiffusivity, &
      ! for relaxation-matrices, we store only the non-zero part (= diagonals)
      mRelaxVecFluid(0:8), mRelaxVecTemp(0:4)
    ! ... GPU (only the vectors)
    real(RK), device, allocatable :: &
         mRelaxVecFluid_d(:), mRelaxVecTemp_d(:)

    ! internal model arrays
    ! NOTES: - last dimension is for 2-lattice alternation
    !        - 1st dimension: 0-8 veloDFs, 9-13 tempDFs
    ! ... CPU
    real(RK), dimension(:,:,:,:), allocatable :: mDFs
    ! ... GPU
    real(RK), dimension(:,:,:,:), device, allocatable :: mDFs_d
    ! Arrays to store macroscopic fields (mainly for simulation-output)
    ! ... CPU
    real(R_SP), dimension(:,:), allocatable, public :: mRawMomDRho, &
         mRawMomUx, mRawMomUy, mRawMomTemp
    ! ... GPU
    real(R_SP), dimension(:,:), device, allocatable, public :: &
         mRawMomDRho_d, mRawMomUx_d, mRawMomUy_d, mRawMomTemp_d

    integer(I4b) :: mOld, mNew, & ! for tracking most recent lattice
         mNx, mNy ! mesh-size (received from 'sim'-class)

  contains
    private
    procedure, public :: init => initMrtSolverBoussinesq2D
    procedure, public :: advanceTime => advanceTimeMrtSolverBoussinesq2D
    procedure, public :: saveCheckpoint => saveCheckpointMrtSolverBoussinesq2D
    procedure, public :: loadCheckpoint => loadCheckpointMrtSolverBoussinesq2D
    procedure, public :: cleanup => cleanupMrtSolverBoussinesq2D
  end type MrtSolverBoussinesq2D

contains
  attributes(global) subroutine solverGPUKernel( &
       relaxVecFluid, relaxVecTemp, &
       alphaG, aParam, diffusivity, &
       DFs, &
       rawMomDRho, rawMomUx, rawMomUy, rawMomTemp, &
       old, new, &
       nX, nY, nTotalLatticeNodes )
    implicit none
    ! kerenel arguments
    real(RK), intent(in) :: &
         relaxVecFluid(0:), relaxVecTemp(0:)
    real(RK), intent(inout) :: DFs(0:, 1:, 0:, 0:)
    real(RK), value, intent(in) :: alphaG, aParam, diffusivity
    real(R_SP), intent(out) :: &
         rawMomDRho(1:, 1:), &
         rawMomUx(1:, 1:), rawMomUy(1:, 1:), &
         rawMomTemp(1:, 1:)
    integer(I4b), value, intent(in) :: old, new, nX, nY, &
         nTotalLatticeNodes
    ! local vars
    integer(I4b) :: x, y, i, threadGlobalIdx
    integer(I4b) :: destX(0:8), destY(0:8)
    real(RK) :: fluidMoms(0:8), tempMoms(0:4), &
         fluidEqMoms(0:8), tempEqMoms(0:4), &
         dfsFluidOld(0:8), dfsTempOld(0:4), &
         dRho, uX, uY, temp, uX2, uY2
    integer(I4b), parameter :: Lx = 1, Ly = 0
    ! BEGIN :: Lattice Constants
    integer(I4b), dimension(0:4), parameter :: OPPOSITE_TEMP = &
         [0, 3, 4, 1, 2]
    integer(I4b), dimension(0:8), parameter :: OPPOSITE_FLUID = &
         [0, 3, 4, 1, 2, 7, 8, 5, 6]
    integer(I4b), dimension(2, 0:8), parameter :: EV_FLUID = reshape( &
         source = [ &
         0,   0, &
         1,   0, &
         0,   1, &
         -1,   0, &
         0,  -1, &
         1,   1, &
         -1,   1, &
         -1,  -1, &
         1,  -1],&
         shape = [2, 9])
    ! END :: Lattice Constants

    ! NOTE We assume a 1D grid, of 1D thread-blocks!
    ! NOTE Globally, we index the threads starting from ZERO (C-like convention!)
    threadGlobalIdx = (blockIdx%x-1) * blockDim%x + (threadIdx%x-1)

    ! IMMEDIATELY EXCLUDE SUPERFLUOUS GPU-THREADS!
    ! NOTE: A GPU-thread is considered superfluous if EITHER
    !       of the following two conditions is met:
    ! (1) IF(=threadGlobalIdx=  >=  =nTotalLatticeNodes=)
    !     This will usually happen, because of the binning of threads into
    !     a rectangular {1/2/3}D grid of rectangular {1/2/3}D thread-blocks,
    !     which means that we will normally have some extra threads that
    !     are "filler". However, the number of filler-threads should be small
    !     (if we divide the grid-of-threadblocks well), so the filler-threads
    !     shouldn't affect performance that much.
    !     NOTE even inside CUDA-Fortran kernels, the smallest
    !          =threadIdx%{x,y,z}= will be ZERO (not ONE), which means that
    !          the =threadGlobalIdx= will be in the range:
    !          [0, (nTotalLatticeNodes - 1)].
    !
    ! (2) IF(thisNode /= fluid)
    !     This is relevant e.g. for bounce-back BCs, where (in the non-sparse
    !     implementations) we do allocate the DFs even for the "solid" nodes,
    !     but we don't want to compute anything there (instead, we want to
    !     let the "fluid"-nodes use that memory for buffering DFs so that they
    !     implement the bounce-back (BB) boundary-condition (BC).

    ! enforce condition (1)
    ! NOTE SURPRISINGLY, the first threadGlobalIdx seems to be 1, NOT 0!
    if( threadGlobalIdx < nTotalLatticeNodes ) then  ! enforce condition (1)

       ! Calculate the {x,y}-coordinates of the node, s.t. we can evaluate
       ! condition (2)
       x = mod(threadGlobalIdx, nX) + Lx
       y = (threadGlobalIdx / nX) + Ly

       ! enforce condition (2)
       ! NOTE for simple geometries, we can write a compact expression for
       !      condition (2). However, in more complex cases (e.g. WDOC-COMPLEX)
       !      we need to look at the geometry-mask!!
       if( (y > 0) .and. (y < (nY-1)) ) then

          ! initializations
          fluidMoms = 0._RK
          tempMoms = 0._RK
          fluidEqMoms = 0._RK
          tempEqMoms = 0._RK

          ! Pre-compute destination-coords...
          ! (a) *IGNORING* periodic BCs...
          destX(0:) = x + EV_FLUID(1, 0:)
          destY(0:) = y + EV_FLUID(2, 0:)
          ! (b) correct for periodic BCs
          do i=1, 8
             ! enforce periodicity :: X
             if( destX(i) > nX ) then
                destX(i) = 1
             elseif( destX(i) < 1 ) then
                destX(i) = nX
             end if
          end do

          ! write: DF-arrays --> local vars
          dfsFluidOld = DFs(0:8, x, y, old)
          dfsTempOld = DFs(9:13, x, y, old)

          ! update local moments
          ! ... fluid
          ! do i=0, 8
          !    fluidMoms(i) = dot_product( M_FLUID(:,i), dfsFluidOld )
          ! end do
          ! NOTE computationally faster expansion in file below
#include "dfs_to_moments_fluid.incf90"

          ! ... temperature
          ! do i=0, 4
          !    tempMoms(i) = dot_product( N_TEMP(:,i), dfsTempOld )
          ! end do
          ! NOTE computationally faster expansion in file below
#include "dfs_to_moments_temperature.incf90"

          ! add 1st-half of force term (Strang splitting)
          fluidMoms(2) = fluidMoms(2) + alphaG/2*tempMoms(0)

          ! save moments related to output
          rawMomDRho(x, y) = real(fluidMoms(0), R_SP)
          rawMomUx(x, y) = real(fluidMoms(1), R_SP)
          rawMomUy(x, y) = real(fluidMoms(2), R_SP)
          rawMomTemp(x, y) = real(tempMoms(0), R_SP)

          ! update local equilibrium moments
          ! ...update some local vars (for readability)
          dRho = fluidMoms(0)
          uX = fluidMoms(1)
          uY = fluidMoms(2)
          temp = tempMoms(0)
          uX2 = uX**2
          uY2 = uY**2
          ! ...fluid
          fluidEqMoms(0) = dRho
          fluidEqMoms(1) = uX
          fluidEqMoms(2) = uY
          fluidEqMoms(3) = -2._RK*dRho + 3._RK*(uX2+uY2)
          fluidEqMoms(4) = uX2 - uY2
          fluidEqMoms(5) = uX*uY
          fluidEqMoms(6) = -uX
          fluidEqMoms(7) = -uY
          fluidEqMoms(8) = -fluidEqMoms(3) - dRho
          ! ...temp
          tempEqMoms(0) = temp
          tempEqMoms(1) = temp*uX
          tempEqMoms(2) = temp*uY
          tempEqMoms(3) = aParam*temp
          tempEqMoms(4) = 0._RK

          ! collision (in moment-space)
          fluidMoms = fluidMoms - relaxVecFluid * (fluidMoms - fluidEqMoms)
          tempMoms  = tempMoms  - relaxVecTemp * (tempMoms - tempEqMoms)

          ! add 2nd-half of force term (Strang splitting)
          fluidMoms(2) = fluidMoms(2) + alphaG/2*tempMoms(0)

          ! map moments back onto DFs...
          ! ...fluid
          ! do i=0, 8
          !    dfsFluidOld = dot_product( M_INV_FLUID(:, i), fluidMoms )
          ! end do
          ! NOTE computationally faster expansion in file below
#include "moments_to_dfs_fluid.incf90"

          ! ...temp
          ! do i=0, 4
          !    dfsTempOld = dot_product( N_INV_TEMP(:, i), tempMoms )
          ! end do
          ! NOTE computationally faster expansion in file below
#include "moments_to_dfs_temperature.incf90"

          ! stream to new array...
          ! ...fluid
          ! ... ... rest
          ! STREAM (also storing runaway DFs in Y-buffer space)
          DFs(0, x, y, new) = dfsFluidOld(0)
          ! ... ... NON-rest
          do i=1, 8
             ! STREAM (also storing runaway DFs in Y-buffer space)
             DFs(i, destX(i), destY(i), new) = dfsFluidOld(i)

             ! apply bounce-back @{bottom & top}
             if( (EV_FLUID(2, i) /= 0) .and. &
                  ((destY(i) == 0) .or. (destY(i) == (nY-1))) ) then
                DFs(OPPOSITE_FLUID(i), x, y, new) = &
                     DFs(i, destX(i), destY(i), old)
             end if
          end do
          ! ...temp
          ! ... ... rest
          ! STREAM (also storing runaway DFs in Y-buffer space)
          DFs(0+9, x, y, new) = dfsTempOld(0)
          ! ... ... NON-rest
          do i=1, 4
             ! STREAM (also storing runaway DFs in Y-buffer space)
             DFs(i+9, destX(i), destY(i), new) = dfsTempOld(i)

             if( destY(i) == 0 ) then
                ! apply Yoshida's scheme @bottom
                DFs(OPPOSITE_TEMP(i)+9, x, y, new) = &
                     -DFs(i+9, destX(i), destY(i), old) + &
                     2._RK*sqrt(3._RK)*diffusivity*TEMP_HOT_WALL
             elseif( destY(i) == (nY-1) ) then
                ! apply Yoshida's scheme @top
                DFs(OPPOSITE_TEMP(i)+9, x, y, new) = &
                     -DFs(i+9, destX(i), destY(i), old) + &
                     2._RK*sqrt(3._RK)*diffusivity*TEMP_COLD_WALL
             end if
          end do
       end if
    end if
  end subroutine solverGPUKernel

  subroutine initMrtSolverBoussinesq2D( this, nX, nY, &
      alphaG, viscosity, diffusivity )
    class(MrtSolverBoussinesq2D), intent(inout) :: this
    real(RK), intent(in) :: alphaG, viscosity, diffusivity
    real(RK) :: sNuEpsE, sQ
    integer(I4b), intent(in) :: nX, nY
    integer(I4b) :: x, y, i, old, new ! dummy vars
    integer(I4b) :: destX(0:8), destY(0:8)
    ! temporary moments-vars
    real(RK) :: fluidMoms(0:8), tempMoms(0:4), tempPerturbation, &
         dfsFluidOld(0:8), dfsTempOld(0:4), rand_value
    integer(I4b) :: rng_seed_length
    integer(I4b), dimension(:), allocatable :: rng_seed

    ! For reproducible RNG-seeding
    call random_seed(size=rng_seed_length)
    allocate( rng_seed(1:rng_seed_length) )
    rng_seed = [ (i, i=1, rng_seed_length) ]
    call random_seed(put=rng_seed)

    ! copy argument-values internally
    this%mNx = nX; this%mNy = nY
    this%mAlphaG = alphaG
    this%mDiffusivity = diffusivity

    ! evaluate the vectors with relaxation-values
    ! ... constants (for brevity)
    sNuEpsE = 2._RK / (6._RK*viscosity + 1._RK)
    sQ = 8._RK * ( (2._RK-sNuEpsE) / (8._RK-sNuEpsE) )
    this%mAParam = ( (60._RK*diffusivity) / sqrt(3._RK) ) - 4._RK
    ! ... ... Enforce safety-check: for stability, we need $a < 1$
    if( this%mAParam >= 1._RK ) then
       write(*,'(4(a,/),a)') "ERROR: invalid combination of model-parameters", &
            "Possible cause: Prandtl-number too low", &
            "Possible fix: increase Pr, or maxMach", &
            "(but later approach increases model-errors)", &
            "Aborting..."
       stop
    end if
    ! ... fluid
    this%mRelaxVecFluid = [ 0._RK, (1._RK, i=1,2), (sNuEpsE, i=1,3), (sQ, i=1,2), sNuEpsE ]
    ! ... temperature
    this%mRelaxVecTemp = [ 0._RK, (SIGMA_K, i=1,2), (SIGMA_NU_E, i=1,2) ]

    tempPerturbation = TEMP_HOT_WALL / 1.E5_RK

    ! get memory for model-state arrays (and Y-buffers)
    allocate( this%mDFs(0:13, 1:this%mNx, 0:(this%mNy+1), 0:1) )
    allocate( this%mRawMomDRho(this%mNx, this%mNy) )
    allocate( this%mRawMomUx(this%mNx, this%mNy) )
    allocate( this%mRawMomUy(this%mNx, this%mNy) )
    allocate( this%mRawMomTemp(this%mNx, this%mNy) )

    ! initialize
    do y=1, this%mNy
       ! ...DFs
       this%mDFs(:, :, y, :) = 0._RK
       ! ...macros
       this%mRawMomDRho(:, y) = 0._RK
       this%mRawMomUx(:, y) = 0._RK
       this%mRawMomUy(:, y) = 0._RK
       this%mRawMomTemp(:, y) = 0._RK
    end do

    ! init tracking-vars for lattice-alternation
    this%mOld = 0
    this%mNew = 1
    ! local aliases (for readability)
    old = this%mOld
    new = this%mNew

    ! ICs for model's state-arrays
    do y=1, this%mNy
       ! Pre-compute destination-coords
       destY = y + EV_FLUID(2, :)

       do x=1, this%mNx
          ! write: DF-arrays --> local vars
          dfsFluidOld = this%mDFs(0:8, x, y, old)
          dfsTempOld = this%mDFs(9:13, x, y, old)
          ! reset moments-vectors
          fluidMoms = 0._RK
          tempMoms = 0._RK
          ! Initialize pressure with steady-state (quadratic) profile, to avoid
          ! the initial oscillations.
          ! fluidMoms(0) = &
          !      (3._RK*this%mAlphaG)/(2._RK*this%mNy)*(y-0.5_RK)*(this%mNy+0.5_RK-y)

          ! Initialize temperature with steady-state (linear) profile, to save
          ! CPU-time. Also here, we insert small perturbation, to break the
          ! symmetry of the system (otherwise, the simulation is too stable).
          !tempMoms(0) = 1._RK - (2._RK*y-1._RK)/(2._RK*this%mNy)
          tempMoms(0) = 0.5_RK
          if( (y >= 2) .and. (y <= this%mNy) ) then
          !if( (x == this%mNx/2+1) .and. (y == 2) ) then
          !   tempMoms(0) = tempMoms(0) + tempPerturbation
          !else
             call random_number(rand_value)
             ! add small amount of noise everywhere
             tempMoms(0) = tempMoms(0) + rand_value*(tempPerturbation / 1000)
          end if

          ! map moments onto DFs...
          ! ...fluid
          do i=0, 8
             dfsFluidOld = dot_product(M_INV_FLUID(:,i), fluidMoms)
          end do
          ! NOTE computationally faster expansion in file below
! #include "expanded_expressions/moments_to_dfs_fluid.incf90"
          ! ...temp
          do i=0, 4
             dfsTempOld = dot_product(N_INV_TEMP(:,i), tempMoms)
          end do
          ! NOTE computationally faster expansion in file below
! #include "expanded_expressions/moments_to_dfs_temperature.incf90"

          ! write: local vars --> DF-arrays
          this%mDFs(0:8, x, y, old) = dfsFluidOld
          this%mDFs(9:13, x, y, old) = dfsTempOld

          ! Pre-compute destination-coords (*IGNORING* periodic BCs for now...will
          ! compensate for that later)
          destX = x + EV_FLUID(1, :)
          do i=1, 8
             ! enforce periodicity :: X
             if( destX(i) > this%mNx ) then
                destX(i) = 1
             elseif( destX(i) < 1 ) then
                destX(i) = this%mNx
             end if
          end do

          ! Fill buffers for bounce-back (for initial time-step)
          ! ...fluid
          do i=1, 8
             if( (destY(i) == 0) .or. (destY(i) == this%mNy+1) ) then
                this%mDFs(i, destX(i), destY(i), old) = dfsFluidOld(i)
             end if
          end do

          ! ...temp
          do i=1, 4
             if( (destY(i) == 0) .or. (destY(i) == this%mNy+1) ) then
                this%mDFs(i+9, destX(i), destY(i), old) = dfsTempOld(i)
             end if
          end do

          ! save ICs
          this%mRawMomDRho(x, y) = real(fluidMoms(0), R_SP)
          this%mRawMomUx(x, y) = real(fluidMoms(1), R_SP)
          this%mRawMomUy(x, y) = real(fluidMoms(2), R_SP)
          this%mRawMomTemp(x, y) = real(tempMoms(0), R_SP)
       end do
    end do

    ! Allocate arrays @GPU
    allocate( this%mRelaxVecFluid_d(0:8), this%mRelaxVecTemp_d(0:4), &
         this%mDFs_d(0:13, 1:this%mNx, 0:(this%mNy+1), 0:1), &
         this%mRawMomDRho_d(this%mNx, this%mNy), &
         this%mRawMomUx_d(this%mNx, this%mNy), &
         this%mRawMomUy_d(this%mNx, this%mNy), &
         this%mRawMomTemp_d(this%mNx, this%mNy) )
    ! Copy-over data to GPU (only what I need, i.e. no raw-moments)
    this%mRelaxVecFluid_d = this%mRelaxVecFluid
    this%mRelaxVecTemp_d = this%mRelaxVecTemp
    this%mDFs_d = this%mDFs
  end subroutine initMrtSolverBoussinesq2D

  ! advance solver-state by one time-step (core LBM-algorithm)
  subroutine advanceTimeMrtSolverBoussinesq2D( this, isTimeToWriteOutput )
    class(MrtSolverBoussinesq2D), intent(inout) :: this
    logical, intent(in) :: isTimeToWriteOutput
    ! local vars
    integer(I4b), parameter :: threads_per_block = 32
    integer(I4b) :: n_thread_blocks

    ! Compute Execution Params
    n_thread_blocks = &
         ceiling( real(this%mNx * (this%mNy+2) ) / threads_per_block )
    ! Launch kernel @GPU!!!
    call solverGPUKernel<<<n_thread_blocks, threads_per_block>>>( &
         this%mRelaxVecFluid_d, this%mRelaxVecTemp_d, &
         this%mAlphaG, this%mAParam, this%mDiffusivity, &
         this%mDFs_d, &
         this%mRawMomDRho_d, &
         this%mRawMomUx_d, this%mRawMomUy_d, &
         this%mRawMomTemp_d, &
         this%mOld, this%mNew, &
         this%mNx, (this%mNy+2), &
         this%mNx * (this%mNy+2) )

    ! swap 'pointers' (for lattice-alternation)
    call swap(this%mOld, this%mNew)

    ! If we need to write output during this iteration, we need to transfer
    ! the raw-macros back to the host!
    if( isTimeToWriteOutput ) then
       this%mRawMomDRho = this%mRawMomDRho_d
       this%mRawMomUx = this%mRawMomUx_d
       this%mRawMomUy = this%mRawMomUy_d
       this%mRawMomTemp = this%mRawMomTemp_d
    end if
  end subroutine advanceTimeMrtSolverBoussinesq2D

  subroutine cleanupMrtSolverBoussinesq2D( this )
    class(MrtSolverBoussinesq2D), intent(inout) :: this
    ! release memory
    ! ... CPU
    deallocate( this%mDFs, this%mRawMomDRho, this%mRawMomUx, this%mRawMomUy, &
         this%mRawMomTemp )
    ! ... GPU
    ! deallocate( this%mRelaxVecFluid_d, this%mRelaxVecTemp_d, &
    !      this%mDFs_d, this%mRawMomDRho_d, this%mRawMomUx_d, &
    !      this%mRawMomUy_d, this%mRawMomTemp_d )
  end subroutine cleanupMrtSolverBoussinesq2D

  ! Save simulation-state to checkpoint-file =fileName=
  ! NOTE We only need to store the *OLD* DFs!
  subroutine saveCheckpointMrtSolverBoussinesq2D( this, fileName )
    ! interface
    class(MrtSolverBoussinesq2D), intent(in) :: this
    character(len=*), intent(in) :: fileName
    ! local vars
    integer(I4b) :: ncID, &
         qDimID, xDimID, yDimID, &
         qVarID, xVarID, yVarID, &
         mDFsVarID
    integer(I4b) :: x, y, i

    ! Copy-over data to HOST
    this%mDFs = this%mDFs_d

    call nfgelb_create( &
         trim(adjustl(fileName)), &  ! =path=
         ior(NF_CLOBBER, NF_64BIT_OFFSET), &  ! =cmode=
         ncID)  ! =ncid=

    ! GLOBAL attributes
    call nfgelb_put_global_att_text(ncID, "Purpose", "GeLB checkpoint-file")
    call nfgelb_put_global_att_1_double(ncID, "mAlphaG", this%mAlphaG)
    call nfgelb_put_global_att_1d_double(ncID, "mRelaxVecFluid", &
         this%mRelaxVecFluid)
    call nfgelb_put_global_att_1d_double(ncID, "mRelaxVecTemp", &
         this%mRelaxVecTemp)
    call nfgelb_put_global_att_1_int(ncID, "mOld", this%mOld)

    ! define dimensions
    qDimID = nfgelb_define_dimension(ncID, "q", 14)
    xDimID = nfgelb_define_dimension(ncID, "x", this%mNx)
    ! NOTE need to store the BUFFERs for bounce-back too!
    yDimID = nfgelb_define_dimension(ncID, "y", this%mNy+2)

    ! define coordinates
    qVarID = nfgelb_define_var_1d_int(ncID, "q", qDimID)
    xVarID = nfgelb_define_var_1d_int(ncID, "x", xDimID)
    yVarID = nfgelb_define_var_1d_int(ncID, "y", yDimID)

    ! define variables (for DFs)
    mDFsVarID = nfgelb_define_var_3d_double(ncID, "mDFs", &
         [qDimID, xDimID, yDimID])

    ! end define-mode (informs NetCDF that we finished defining metadata)
    call nfgelb_end_define_mode(ncID)

    ! write data
    ! ... coordinates
    call nfgelb_put_entire_var_1d_int(ncID, qVarID, [(i, i=0, 13)])  ! qVals
    call nfgelb_put_entire_var_1d_int(ncID, xVarID, &
         [ (x, x=1, this%mNx) ])  ! xVals
    ! NOTE need to store the BUFFERs for bounce-back too!
    call nfgelb_put_entire_var_1d_int(ncID, yVarID, &
         [ (y, y=0, (this%mNy+1)) ])  ! yVals
    ! ... DFs
    call nfgelb_put_entire_var_3d_double(ncID, mDFsVarID, &
         this%mDFs(:,:,:,this%mOld))

    call nfgelb_close(ncID)
  end subroutine saveCheckpointMrtSolverBoussinesq2D

  ! Load simulation-state from checkpoint-file =fileName=
  subroutine loadCheckpointMrtSolverBoussinesq2D( this, fileName )
    ! interface
    class(MrtSolverBoussinesq2D), intent(inout) :: this
    character(len=*), intent(in) :: fileName
    ! local vars
    integer(I4b) :: ncID, xFirst, xLast, yFirst, yLast
    ! ... data-destinations
    integer(I4b), dimension(:), allocatable :: qVals, xVals, yVals

    call nfgelb_open( &
         trim(adjustl(fileName)), &  ! =path=
         NF_NOWRITE, &  ! =cmode=
         ncID)  ! =ncid=

    ! read dimensions
    qVals = nfgelb_read_dimension_array_int(ncID, "q")
    xVals = nfgelb_read_dimension_array_int(ncID, "x")
    yVals = nfgelb_read_dimension_array_int(ncID, "y")
    ! evaluate =<*>First= and =<*>Last= along each dimension
    ! NOTE: for =q=, we already know the bounds, so no need to re-calculate
    xFirst = xVals(1); xLast = xVals(size(xVals))
    yFirst = yVals(1); yLast = yVals(size(yVals))

    ! read global attributes
    this%mAlphaG = nfgelb_get_global_att_1_double(ncID, "mAlphaG")
    this%mRelaxVecFluid = nfgelb_get_global_att_1d_double(ncID, &
         "mRelaxVecFluid")
    this%mRelaxVecTemp = nfgelb_get_global_att_1d_double(ncID, &
         "mRelaxVecTemp")
    this%mOld = nfgelb_get_global_att_1_int(ncID, "mOld")
    this%mNew = 1 - this%mOld

    ! allocate space for DFs
    if( .not. allocated(this%mDFs) ) then
       allocate( this%mDFs( 0 : 13, &
            xFirst : xLast, yFirst : yLast, 0:1) )
    end if
    ! read DF fields
    call nfgelb_get_entire_var_3d_double(ncID, "mDFs", &
         0, xFirst, yFirst, &
         this%mDFs(:,:,:, this%mOld))

    call nfgelb_close(ncID)
  end subroutine loadCheckpointMrtSolverBoussinesq2D
end module MrtSolverBoussinesq2D_class
