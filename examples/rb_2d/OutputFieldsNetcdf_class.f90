! File: OutputFieldsNetcdf_class.f90
! Author: Dragos B. Chirila
! Purpose: Write the macroscopic fields to a NetCDF file.

module OutputFieldsNetcdf_class
  use NumericKinds, only : I4b, I8b, RK, R_SP, R_DP
  use NetCDFUtils
  use RunTimeUtils
  implicit none

  ! string-constants for output metadata
  character(len=*), parameter :: UNITS_STR="units", & ! for global-attribute
       SPACE_UNITS_STR="char. length", TIME_UNITS_STR="char. time", &
       PRESS_UNITS_STR="char. pressure-difference", &
       VEL_UNITS_STR="char. velocity", TEMP_UNITS_STR="char. temperature-difference", &
       KE_UNITS_STR="(char. velocity)**2"

  type :: OutputFieldsNetcdf
     private
     real(R_DP) :: mUyMax, mTKE
     character(len=256) :: mOutFilePrefix

     ! information about the simulation
     integer(I4b) :: mNx, mNy
     integer(I8b) :: mNumItersMax, mOutputFrequency
     real(R_SP) :: mDxD, mDtD, mRa, mPr, mMaxMach
     integer(I4b) :: mCurrOutSlice ! for tracking output time-slices

     ! conversion-factors for translating output from numerical- to
     ! dimensionless-units
     real(RK) :: mDRhoSolver2PressDimless, mVelSolver2VelDimless

     ! internal handlers for netCDF objects
     integer(I4b) :: mNcID, mPressVarID, mUxVarID, mUyVarID, mTempVarID, &
          mUyMaxVarID, mTKEVarID, mTimeVarID
   contains
     private
     ! public methods which differ from base-class analogues
     procedure, public :: init => initOutputFieldsNetcdf
     procedure, public :: writeOutput => writeOutputFieldsNetcdf
     procedure, public :: cleanup => cleanupOutputFieldsNetcdf
     procedure, public :: isTimeToWrite
     ! internal method
     procedure prepareFileOutputFieldsNetcdf
  end type OutputFieldsNetcdf

contains
  subroutine initOutputFieldsNetcdf( this, nX, nY, outputFrequency, dxD, dtD, &
       nItersMax, outFilePrefix, Ra, Pr, maxMach )
    class(OutputFieldsNetcdf), intent(inout) :: this
    integer(I4b), intent(in) :: nX, nY
    integer(I8b), intent(in) :: outputFrequency, nItersMax
    real(RK), intent(in) :: dxD, dtD, Ra, Pr, maxMach
    character(len=*), intent(in) :: outFilePrefix

    call validate_output_frequency( outputFrequency )

    this%mOutputFrequency = outputFrequency

    if( outputFrequency == 0 ) then
       write(*,'(a)') "<<<INFO>>> no file-output, due of 'outputFrequency'"
    else  ! outputFrequency > 0, so prepare output
       ! copy over remaining arguments into internal-state
       this%mNx = nX; this%mNy = nY
       this%mNumItersMax = nItersMax
       this%mDxD = real(dxD, R_SP)
       this%mDtD = real(dtD, R_SP)
       this%mRa = real(Ra, R_SP)
       this%mPr = real(Pr, R_SP)
       this%mMaxMach = real(maxMach, R_SP)
       this%mOutFilePrefix = outFilePrefix

       ! conversion-factors for output
       this%mVelSolver2VelDimless = dxD / dtD
       this%mDRhoSolver2PressDimless = this%mVelSolver2VelDimless**2 / 3._RK

       this%mCurrOutSlice = 0

       call this%prepareFileOutputFieldsNetcdf()
    end if
  end subroutine initOutputFieldsNetcdf

  subroutine prepareFileOutputFieldsNetcdf( this )
    class(OutputFieldsNetcdf), intent(inout) :: this
    ! local vars
    ! Variables to store temporary IDs returned by the netCDF library; no need
    ! to save these, since they are only needed when writing the file-header.
    ! NOTES: - we have 3 dimension IDs (2D=space + 1D=time)
    !        - HOWEVER, there is no 'tVarID', since this ID is needed later (to
    !          append values to this UNLIMITED-axis), so it is stored in the
    !          internal state of the type (in 'mTimeVarID')
    integer(I4b) :: spaceTimeDimIDs(3), xDimID, yDimID, tDimID, &
         xVarID, yVarID
    integer(I4b) :: x, y

    call nfgelb_create( &
         trim(adjustl(this%mOutFilePrefix)) // ".nc", &  ! =path=
         ior(NF_CLOBBER, NF_64BIT_OFFSET), &  ! =cmode=
         this%mNcID)  ! =ncid=

    ! GLOBAL attributes
    ! ...attributes for ALL Gauges in this simulation
    call nfgelb_put_global_att_text(this%mNcID, "Conventions", "CF-1.6")
    call nfgelb_put_global_att_1_float(this%mNcID, "Ra", this%mRa)
    call nfgelb_put_global_att_1_float(this%mNcID, "Pr", this%mPr)
    call nfgelb_put_global_att_1_float(this%mNcID, "maxMach", this%mMaxMach)
    ! ...attributes for SOME Gauges in this simulation
    call nfgelb_put_global_att_text(this%mNcID, SPACE_UNITS_STR, &
         "channel height $L$")
    call nfgelb_put_global_att_text(this%mNcID, TIME_UNITS_STR, &
         "diffusive time-scale $\frac{L^2}{\kappa}$")
    call nfgelb_put_global_att_text(this%mNcID, PRESS_UNITS_STR, &
         "$\frac{\rho_0\kappa^2}{L^2}$")
    call nfgelb_put_global_att_text(this%mNcID, VEL_UNITS_STR, &
         "$\frac{\kappa}{L}$")
    call nfgelb_put_global_att_text(this%mNcID, TEMP_UNITS_STR, &
         "temperature-diff / horiz. walls $\theta_b-\theta_t$")

    ! define dimensions (netCDF will return ID for each)
    xDimID = nfgelb_define_dimension(this%mNcID, "x", this%mNx)
    yDimID = nfgelb_define_dimension(this%mNcID, "y", this%mNy)
    tDimID = nfgelb_define_dimension(this%mNcID, "time", NF_UNLIMITED)

    ! define coordinates
    xVarID = nfgelb_define_var_1d_float(this%mNcID, "x", xDimID)
    yVarID = nfgelb_define_var_1d_float(this%mNcID, "y", yDimID)
    this%mTimeVarID = nfgelb_define_var_1d_float(this%mNcID, "time", tDimID)

    ! assign units-attributes to coordinate vars
    call nfgelb_set_var_metadata(this%mNcID, xVarID, units="1", &
         long_name=SPACE_UNITS_STR)
    call nfgelb_set_var_metadata(this%mNcID, yVarID, units="1", &
         long_name=SPACE_UNITS_STR)
    call nfgelb_set_var_metadata(this%mNcID, this%mTimeVarID, &
         units="time since start of simulation", long_name=TIME_UNITS_STR)

    ! spaceTimeDimIDs-array is used for passing the IDs corresponding to the dimensions
    ! of the variables
    spaceTimeDimIDs = [ xDimID, yDimID, tDimID ]

    ! define the variables
    ! ...to save space, we store most results as =float=
    this%mPressVarID = nfgelb_define_var_3d_float(this%mNcID, "press_diff", &
         spaceTimeDimIDs)
    this%mTempVarID = nfgelb_define_var_3d_float(this%mNcID, "temp_diff", &
         spaceTimeDimIDs)
    this%mUxVarID = nfgelb_define_var_3d_float(this%mNcID, "u_x", &
         spaceTimeDimIDs)
    this%mUyVarID = nfgelb_define_var_3d_float(this%mNcID, "u_y", &
         spaceTimeDimIDs)
    ! ...HOWEVER, for the {'mUyMax', 'mTKE'}-fields, we need =double=
    this%mUyMaxVarID = nfgelb_define_var_1d_double(this%mNcID, "max_u_y", tDimID)
    this%mTKEVarID = nfgelb_define_var_1d_double(this%mNcID, "tke", tDimID)

    ! assign units-attributes to output-variables
    call nfgelb_set_var_metadata(this%mNcID, this%mPressVarID, units="1", &
         long_name=PRESS_UNITS_STR)
    call nfgelb_set_var_metadata(this%mNcID, this%mTempVarID, units="1", &
         long_name=TEMP_UNITS_STR)
    call nfgelb_set_var_metadata(this%mNcID, this%mUxVarID, units="1", &
         long_name=VEL_UNITS_STR)
    call nfgelb_set_var_metadata(this%mNcID, this%mUyVarID, units="1", &
         long_name=VEL_UNITS_STR)
    call nfgelb_set_var_metadata(this%mNcID, this%mUyMaxVarID, units="1", &
         long_name=VEL_UNITS_STR)
    call nfgelb_set_var_metadata(this%mNcID, this%mTKEVarID, units="1", &
         long_name=KE_UNITS_STR)

    ! end define-mode (informs netCDF we finished defining metadata)
    call nfgelb_end_define_mode(this%mNcID)

    ! write data (but only for coordinates which are NOT UNLIMITED)
    call nfgelb_put_entire_var_1d_float(this%mNcID, xVarID, &
         [ (real(this%mDxD*(x-0.5)), x=1, this%mNx) ])  ! XVals
    call nfgelb_put_entire_var_1d_float(this%mNcID, yVarID, &
         [ (real(this%mDxD*(y-0.5)), y=1, this%mNy) ])  ! YVals
  end subroutine prepareFileOutputFieldsNetcdf

  subroutine writeOutputFieldsNetcdf( this, rawMomDRho, &
       rawMomUx, rawMomUy, rawMomTemp, iterNum )
    class(OutputFieldsNetcdf), intent(inout) :: this
    real(R_SP), dimension(1:this%mNx, 1:this%mNy), intent(inout) :: &
         rawMomDRho, rawMomUx, rawMomUy, rawMomTemp
    integer(I8b), intent(in) :: iterNum
    ! local variables
    real(R_SP) :: currTime
    integer(I8b) :: x, y

    if( this%isTimeToWrite( iterNum ) ) then
       ! increment output time-slice if it is time to generate output
       this%mCurrOutSlice = this%mCurrOutSlice + 1

       ! Evaluate current dimensionless-time (0.5 due to Strang-splitting)
       if( iterNum == 0 ) then
          currTime = 0._R_SP
       else
          currTime = real( (iterNum-0.5_R_SP)*this%mDtD, R_SP )
       end if
       ! append value to UNLIMITED time-dimension
       call nfgelb_append_1_to_var_1d_float(this%mNcID, this%mTimeVarID, &
            this%mCurrOutSlice, currTime)

       this%mUyMax = -huge(R_DP)
       this%mTKE = 0._R_DP

       ! explicitly scale the output-data (on some compilers it doesn't
       ! work if we scale directly when we pass the function-arg; also,
       ! this may be more efficient even for =gfortran=, because we ensure
       ! that the scaling is done "in-place")
       do y=1, this%mNy
          do x=1, this%mNx
             rawMomDRho(x, y) = &
                  real(rawMomDRho(x, y)*this%mDRhoSolver2PressDimless, R_SP)
             rawMomUx(x, y) = &
                  real(rawMomUx(x, y)*this%mVelSolver2VelDimless, R_SP)
             rawMomUy(x, y) = &
                  real(rawMomUy(x, y)*this%mVelSolver2VelDimless, R_SP)
             rawMomTemp(x, y) = rawMomTemp(x, y) - 0.5

             ! for finding the UyMax
             if( abs(rawMomUy(x, y)) .gt. this%mUyMax ) then
                this%mUyMax = abs(rawMomUy(x, y))
             end if

             ! for accumulating the TKE
             this%mTKE = this%mTKE + ( &
                  rawMomUx(x, y)**2 + rawMomUy(x, y)**2 &
                  )
          end do
       end do
       ! WARNING !
       ! WARNING !
       ! WARNING !
       ! I know why this doesn't work as expected (i.e. a message every 10% of
       ! tSim). The problem is that iterNum is sampling in a discrete way the
       ! iteration-range (because we don't necessarily write output at every
       ! iteration!).
       if( (this%mNumItersMax <= 10) .or. &
            (mod(iterNum, (this%mNumItersMax / 10)) == 0)) then
          ! write {UyMax, TKE} to terminal also
          write(*,'(a, f0.6, 2(a, en15.6e3))') "<<<OUTPUT>> @time = ", currTime, &
               ", UyMax = ", this%mUyMax, ", TKE = ", this%mTKE
       end if

       ! write data (scaled to dimensionless units) to file
       ! ...dimensionless pressure-difference
       call nfgelb_append_slice_to_var_3d_float(this%mNcID, this%mPressVarID, &
            [1, 1, this%mCurrOutSlice], &
            [this%mNx, this%mNy, 1], &
            rawMomDRho(1:,1:))
       ! ...dimensionless Ux
       call nfgelb_append_slice_to_var_3d_float(this%mNcID, this%mUxVarID, &
            [1, 1, this%mCurrOutSlice], &
            [this%mNx, this%mNy, 1], &
            rawMomUx(1:,1:))
       ! ...dimensionless Uy
       call nfgelb_append_slice_to_var_3d_float(this%mNcID, this%mUyVarID, &
            [1, 1, this%mCurrOutSlice], &
            [this%mNx, this%mNy, 1], &
            rawMomUy(1:,1:))
       ! ...dimensionless temperature-difference
       call nfgelb_append_slice_to_var_3d_float(this%mNcID, this%mTempVarID, &
            [1, 1, this%mCurrOutSlice], &
            [this%mNx, this%mNy, 1], &
            rawMomTemp(1:,1:))
       ! ...max<Uy> (for bifurcation test criterion)
       call nfgelb_append_1_to_var_1d_double(this%mNcID, this%mUyMaxVarID, &
            this%mCurrOutSlice, this%mUyMax)
       ! ...TKE (for checking of statistical convergence)
       call nfgelb_append_1_to_var_1d_double(this%mNcID, this%mTKEVarID, &
            this%mCurrOutSlice, this%mTKE)

       ! SYNC file (s.t. OTHER PROCESSES can READ while the program is WRITING,
       ! e.g. for monitoring & visualizing the simulations)
       call nfgelb_sync_file(this%mNcID)
    end if
  end subroutine writeOutputFieldsNetcdf

  logical function isTimeToWrite( this, iterNum )
    class(OutputFieldsNetcdf), intent(in) :: this
    integer(I8b), intent(in) :: iterNum

    if( this%mOutputFrequency == 0 ) then
       isTimeToWrite = .false.
    else
       isTimeToWrite = (mod(iterNum, this%mOutputFrequency) == 0)
    end if
  end function isTimeToWrite

  subroutine cleanupOutputFieldsNetcdf( this )
    class(OutputFieldsNetcdf), intent(inout) :: this
    if( this%mOutputFrequency > 0 ) then
       call nfgelb_close(this%mNcID)
    end if
  end subroutine cleanupOutputFieldsNetcdf
end module OutputFieldsNetcdf_class
